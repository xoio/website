const path = require('path');


module.exports = {
    entry:'./src/app.ts',
    mode:"production",
    devtool:"source-map",
    module:{

        rules: [
            {
                test:/\.(glsl|vert|frag|vs|fs)$/,
                use:'raw-loader'
            },
            {
                test:/\.(ts|tsx)$/,
                exclude:/(node_modules | bower_components)/,
                use:{
                    loader:'awesome-typescript-loader'
                }
            },
            {
                test:/\.scss$/,
                use:[
                    "style-loader",
                    "css-loader",
                    "sass-loader"
                ]
            }

        ],


    },
    resolve:{
        extensions:[".tsx",".ts",".js",".vert",".frag",".glsl",".scss"]
    },
    output:{
        filename:'app.[name].min.js',
        path:path.resolve(__dirname,"build")
    }
};