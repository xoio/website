const fs = require("fs");
const consts = require('./consts');

module.exports = {

    /**
     * Helps extract project paths from s3 response
     * @param paths {Object} response received when listing project folders
     * @returns {{path: S3.Prefix, id: string}[]} sorted array with just the path and an id.
     */
    sortProjectPaths(paths){
        return paths.CommonPrefixes.map(itm => {
            let path = itm.Prefix;
            path = path.split("/");
            return {
                id:path[1],
                path:itm.Prefix
            }

        });
    },

    /**
     * List folders
     * @param s3 {*} AWS sdk s3 instance
     * @param path {string} path on S3 to the file
     * @param cb {Function} a callback to run when the file is acquired
     */
    getProjectList(s3,path){
        return s3.listObjects({
            Bucket:consts.bucket,
            Delimiter:"/",
            Prefix:`${consts.basePath}/${path}`
        }).promise();
    },

    listFolder(s3,path) {
        return s3.listObjects({
            Bucket: consts.bucket,
            Delimiter: "/",
            Prefix: `${path}`
        }).promise();
    },

    /**
     * Returns content at the specified s3 path.
     * @param s3 {*} AWS sdk s3 instance
     * @param path {string} path on S3 to the file
     *
     */
    getObject(s3,path){
        return s3.getObject({
            Bucket:consts.bucket,
            Key:path,
        }).promise();
    }
};