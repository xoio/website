const fs = require('fs');
const wrench = require('wrench')
const basePath = `${process.cwd()}/build`;
const publicPath = `${process.cwd()}/public`;
const webpack = require('webpack');


/**
 Takes care of the rest of the build - moves assets, etc + makes index.html for everything
 */


// build index page
let index = fs.readFileSync(`${process.cwd()}/public/index.html`,"utf8");
index = index.replace("app.js","app.main.min.js");

fs.writeFile(`${basePath}/index.html`,index,err => {
    if(err){
        throw Error(err);
    }
})

/**
 * check for site assets folder.
 */
if(!fs.existsSync(`${basePath}/assets`)){
    fs.mkdirSync(`${basePath}/assets`);
    syncAssets();
}else{
    syncAssets();
}

/**
 * Check for project content folder.
 */
if(!fs.existsSync(`${basePath}/content`)){
    fs.mkdirSync(`${basePath}/content`);
    syncContent();
}else{
    syncContent();
}

/**
 * Sync site assets
 */
function syncAssets(){
    let src = `${publicPath}/assets`
    let dst = `${basePath}/assets`
    wrench.copyDirSyncRecursive(src,dst,{
        forceDelete: true
    })
}

/**
 * Sync site project content.
 */
function syncContent(){
    let src = `${publicPath}/content`
    let dst = `${basePath}/content`
    wrench.copyDirSyncRecursive(src,dst,{
        forceDelete: true
    })
}
