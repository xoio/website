const AWS = require('aws-sdk')
const fs = require("fs");
const markdown = require("markdown-it");
const helpers = require('./helpers')

/**
 * Pulls data from S3 to build content list.
 */

const md = new markdown();

// need to be explicit for some reason on windows.
AWS.config.credentials =new AWS.SharedIniFileCredentials({
    profile:"default",
    filename:"/mnt/c/Users/sortofsleepy/.aws/credentials"
});

const s3 = new AWS.S3({
    apiVersion:"2006-03-01"
});

// check for data manifest
let manifest = "";
if(!fs.existsSync(`${process.cwd()}/src/data.json`)){
    fs.writeFileSync(`${process.cwd()}/src/data.json`,"");
}else{
    manifest = JSON.parse(fs.readFileSync(`${process.cwd()}/src/data.json`,"utf8"));
}


// =========== KICK THINGS OFF ============== //

// TODO add options to choose which part to update

helpers.getProjectList(s3,"").then(data => {

    let content = helpers.sortProjectPaths(data);

    content.forEach(project => {
        let assets = `${project.path}assets/`;
        let projectPath = `${process.cwd()}/public/content/${project.id}`;
        let contentPath = `projects/${project.id}/content.md`
        let metaPath = `projects/${project.id}/meta.json`
        let thumbPath = `projects/${project.id}/thumb.jpg`

        // append assets array to project object
        project["assets"] = [];
        project["content"] = null;
        project["meta"] = null;
        project["thumb"] = null;

        // start by building out folder to download stuff to if it doesn't exist already.
        if(!fs.existsSync(projectPath)){
            fs.mkdirSync(projectPath);
            fs.mkdirSync(projectPath + "/assets")
        }

        // download project ocntent.
        helpers.getObject(s3,contentPath).then(obj => {
            project["content"] = md.render( obj.Body.toString("utf8"));
        }).catch(e => {
            console.log("error - unable to download content markdown file." + e);
        });

        // download meta.json
        helpers.getObject(s3,metaPath).then(obj => {
            project["meta"] = JSON.parse(obj.Body.toString("utf8"));
        }).catch(e => {
            console.log("error - unable to download content metadata json - " + e);
            console.log("exiting due to inability to complete");
            process.exit();
        });

        // get thumbnail
        helpers.getObject(s3,thumbPath).then(obj => {

            fs.writeFile(`${process.cwd()}/public/content/${project.id}/thumb.jpg`,obj.Body, err => {
                if(err) {
                    console.log("Error saving image for project - ", project.id, " | Error was ", err)
                }else {
                    console.log("Saved thumbnail for ",project.id);
                    project["thumb"] = `/content/${project.id}/thumb.jpg`;
                    //project["thumb"] = obj.Key.replace("projects/",`/content/`)
                }
            })
        }).catch(e => {
            console.log("error - unable to download thumbnail image for thumbPath - " + thumbPath + " " + e);
            console.log("exiting due to inability to complete");
            process.exit();

        });

        // look through assets folder and download images
        helpers.listFolder(s3,assets).then(data => {
            let imgs = data.Contents;

            imgs.forEach(img => {

                let assetPath = img.Key.replace("projects/",`${process.cwd()}/public/content/`);

                // push final client side path to image
                project["assets"].push(
                    img.Key.replace("projects/",`/content/`)
                );

                // download the image.
                helpers.getObject(s3,img.Key).then(obj => {

                    fs.writeFile(assetPath,obj.Body, err => {
                        if(err) {
                            console.log("Error saving image - ", err)
                        }else {
                            console.log("Saved image at - " + assetPath);
                        }
                    })
                }).catch(e => {
                    console.log("Error downloading image " + img.Key + " - ",e);

                })
            });
        }).catch(e => {
            console.log("exiting due to inability to complete download of images - error was ",e);
            process.exit();
        });

        // add everything to the manifest.
        // TODO make it work better since images could take longer to finish.
        let timer = setInterval(() => {
            if(project.content !== null && project.meta !== null && project.assets.length > 1 && project.thumb !== null){

                if(!manifest.hasOwnProperty(project.id)){
                    manifest[project.id] = {};
                }

                if(manifest.hasOwnProperty(project.id)){
                    manifest[project.id].content = project.content;
                    manifest[project.id].meta = project.meta;
                    manifest[project.id].assets = project.assets;
                    manifest[project.id].thumb = project.thumb;
                }

                fs.writeFile(process.cwd() + "/src/data.json",JSON.stringify(manifest), err => {
                    if(err){
                        throw new Error(err);
                    }
                    console.log("Updated manifest");
                })
                clearInterval(timer);
            }
        })

    });



});