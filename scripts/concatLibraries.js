const fs = require('fs');
const compile = require('google-closure-compiler-js')
const argv = require('minimist')(process.argv.slice(2));

const basePath = "./libraries";

let libs = fs.readdirSync('./libraries');

let files = [];

libs.forEach(lib => {
    files.push(fs.readFileSync(`${basePath}/${lib}`));
});

// if building for production - use advanced compilation - otherwise just concat files
if(argv.hasOwnProperty("production")){
    const flags =  {
        compilationLevel:"ADVANCED",
        jsCode:[
            {
                src:files.join("")
            }
        ]
    }

    let out = compile(flags);
    fs.writeFile('./public/libs.min.js',out.compiledCode, err => {
        if(err){
            throw new Error(err);
        }
    });

}else {

    fs.writeFile('./public/libs.min.js',files.join(""), err => {
        if(err){
            throw new Error(err);
        }
    });


}

