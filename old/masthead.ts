import {convertTemplate,charSplit} from './component-helpers'
import {Component} from "../interfaces";

/**
 * This component builds the header and menu.
 */


export default class Masthead implements Component {


    mounted(){}
    tree(vue:any){


        let chars = charSplit(vue.title);

        let blockedChars = chars.map(itm => {
            return `<div class="title">${itm}</div>`
        })


        return [
            [
                "header",
                ["h1",blockedChars.join("")],
                [
                    "nav",
                    []
                ]
            ]
        ]
    }


    render(h: any, vue:any) {
        return convertTemplate(h,this.tree(vue));
    }
}
