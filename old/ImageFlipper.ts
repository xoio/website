import createFbo from './jirachi/core/fbo'
import Mesh from "./jirachi/framework/mesh";
import Plane from "./jirachi/geometry/plane";
import createShader from './jirachi/core/shader'
import createTexture from './jirachi/core/texture'
import {ShaderFormat, TextureFormat} from "./jirachi/core/formats";
import Quad from "./jirachi/geometry/quad";
import Loader from './AssetLoaderr'
import pvert from './shaders/plane.vert'
import pfrag from './shaders/plane.frag'
import fpvert from './shaders/flipperRender.vert'
import fpfrag from './shaders/flipperRender.frag'

export default class ImageFlipper {
    gl:any;

    // assets to flip through
    assets:Array<WebGLTexture> = [];

    // mesh to render the thumbnail
    planeMesh:any;

    // mesh to render the complete image
    planeRenderMesh:any;

    //
    planeShader:any;
    planeRenderShader:any;

    // fbo to render thigns onto
    fbo:any;

    // keeps track of which thumbnail we're on.
    idx:number = 0;

    // resolution of the canvas
    resolution:Array<any> = [];
    width:number;
    height:number;

    // handles image animations.
    threshold:number = 0.03;
    transition:number = 0.5;

    // texture used when transitioning from one image to another.
    transitionTexture:WebGLTexture;

    mixRatio:number = 1.0;

    // flag so we know when things are loaded
    loaded:boolean = false;

    // resolution for thumbnail images.
    imageResolution:[number,number];

    vue:any;

    // stores project order
    projects:Array<any>;

    // whether or not we're animating to the next slide.
    isAnimating:boolean = false;

    // current image to show
    currentImage:any;

    // next image to show
    nextImage:any;

    constructor(gl,vue,imageResolution:[number,number],assets:Array<HTMLImageElement>){
        this.gl = gl;
        this.resolution[0] = gl.canvas.width;
        this.resolution[1] = gl.canvas.height;
        this.imageResolution = imageResolution;

        this.vue = vue;
        this._setup();


        Loader.loadAssets(["./assets/transition.png"]).then(transition =>{

            this.transitionTexture = createTexture(gl,new TextureFormat(gl,{
                data:transition[0]
            }));

            this.assets = assets.map(img =>{
                return createTexture(gl,new TextureFormat(gl,{
                    data:img
                }));
            });

            this._buildProjectOrder();

            this.loaded = true;
            this._resize();
        });

    }

    /**
     * Builds out the project the order
     * @private
     */
    _buildProjectOrder() {
        let vue = this.vue;
        let assets = this.assets;
        let projects = [];

        for(let i in vue.content){
            assets.forEach(asset => {
                let split = asset["format"]["data"]["src"].split("/");
                let id = split[split.length - 2];
                if(id === i){
                    projects.push(asset);
                }
            })

        }

        this.assets = projects;
        this.currentImage =  this.assets[this.vue.projectIndex];
        this.nextImage = this.assets[this.vue.nextProjectIndex];
    }

    /**
     * Return the current output
     */
    getOutput(){
        return this.fbo.getColorTexture();
    }

    /**
     * Tweaks the mix ratio value to allow for the animation transition.
     */
    changeImage(){

        TweenMax.to(this,1.5,{
            mixRatio:0,
            ease:TweenMax.easeInOut,
            onComplete:this._transitionComplete.bind(this)
        })
        
    }

    /**
     * transition complete handler for when we change images.
     * @private
     */
    _transitionComplete(){
        this.mixRatio = 1.0;

      //  console.log("Current is ",this.vue.projectIndex, "|", "Next is ",this.vue.nextProjectIndex)

        this.currentImage =  this.assets[this.vue.projectIndex];
        this.nextImage = this.assets[this.vue.nextProjectIndex];
    }

    update(camera){

        if(this.loaded){
            this.planeMesh.draw(camera,shader => {


                this.currentImage.bind();
                this.nextImage.bind(1);

                // TODO this is actually kinda pointless for some reason,ends up just being a regular cross-fade, not sure why.
                this.transitionTexture["bind"](2);

                shader.int("currentTexture",0);
                shader.int("nextTexture",1);
                shader.int("transitionTexture",2);
                shader.float("mixRatio",this.mixRatio);
                shader.float("threshold",this.threshold);

            });

            // make sure there are no bound textures
            this.gl.unbindTexture();
        }
    }

    _resize(){
        this.fbo.resize(this.gl.canvas.width,this.gl.canvas.height);
        this.planeMesh = new Mesh(this.planeShader,new Plane(this.imageResolution[0],this.imageResolution[1])).setFbo(this.fbo);
    }

    _setup(){
        let gl = this.gl;
        let w = this.imageResolution[0];
        let h = this.imageResolution[1];

        this.fbo = createFbo(gl,new TextureFormat(gl,{
            width:window.innerWidth,
            height:window.innerHeight
        }));
        this.planeShader = createShader(gl,new ShaderFormat(pvert,pfrag));
        this.planeRenderShader = createShader(gl,new ShaderFormat(fpvert,fpfrag))
        this.planeMesh = new Mesh(this.planeShader,new Plane(w,h)).setFbo(this.fbo);

        let quad = new Quad();
        let planeRender = new Mesh(this.planeRenderShader);
        planeRender.addAttribute('position',quad.vertices,{
            size:2
        });
        this.planeRenderMesh = planeRender
    }
}