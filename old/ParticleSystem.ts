import TransformFeedback from '../jirachi/framework/TransformFeedback'
import createShader from '../jirachi/core/shader'
import {flattenArray, randVec3, randVec4} from "../jirachi/math/core";
import {ShaderFormat} from "../jirachi/core/formats";



// import shaders
import vert from './shaders/petalSim.glsl'
import rVert from './shaders/petalRender.vert'
import rFrag from './shaders/petalRender.frag'
import rotate from './jirachi/shaders/utils.glsl'
import noise from './jirachi/shaders/noise/curl.glsl'
import Mesh from "./jirachi/framework/mesh";


export default class ParticleSystem{
    gl:any;
    numParticles:number;
    tf:TransformFeedback;
    renderShader:any;
    petal:Mesh;
    resolution:[number,number];
    constructor(gl,vue,numParticles=100) {

        this.gl = gl;
        this.numParticles = numParticles;

        this.tf = new TransformFeedback(gl,{
            numItems:numParticles
        });

        let simShader = [
            noise,
            vert
        ].join("\n");

        this.resolution = vue.resolution;

        // set transform feedback shader.
        this.tf.setShader(simShader,[
            "oPos",
            "oVel"
        ],[]);


        this.renderShader = createShader(gl,new ShaderFormat()
            .vertexSource([
                rotate,
                rVert
            ].join("\n"))
            .fragmentSource(rFrag));

        this._loadGeometry(vue._data.assets)

    }


    draw(camera){
        let perf = performance.now() * 0.005;
        // TODO enable alpha blending

        // update transform feedback
        this.tf.update();

        this.petal.draw(camera);
        this.petal.shader.float("time",perf);
        this.petal.uniform("projectionMatrix",camera.projectionMatrix)
            .uniform("modelViewMatrix",camera.viewMatrix)
            .uniform("resolution",this.resolution);
    }

    _build(){

        let positions = [];
        let velocity = [];
        let originData = [];

        for(let i = 0; i < this.numParticles; ++i){

            // setup particle position
            let position = randVec4();
            position[0] -= 30.0;

            // setup particle velocity
            let vel = randVec3(0.004 * i);

            positions.push(...position);
            velocity.push(...vel);
            originData.push(position[0],position[1],position[2]);
        }

        // add attributes to keep track of.
        // note - position w will hold particle life.
        this.tf.addAttribute("position",positions,{
            size:4
        });
        this.tf.addAttribute("velocity",velocity);

        this.tf.addAttribute("origin",originData);

    }

    /**
     * Load petal geometry
     * @param assets
     * @private
     */
    _loadGeometry(assets){
        this._build();
        let petalGeo = null;
        assets.forEach(itm => {
            if(itm.name === "petal"){
                petalGeo = itm.data;
            }
        })

        // flatten arrays
        let positions = flattenArray(petalGeo.positions);
        let uvs = flattenArray(petalGeo.coords,2);

        // setup random scale
        let scale = [];
        for(let i = 0; i < this.numParticles;++i){
            scale.push(Math.random() + 1.0);
        }


        this.petal = new Mesh(this.renderShader)
        this.petal.addAttribute("position",positions);
        this.petal.addInstancedAttributeBuffer("iPosition",this.tf.getAttributeData("position"),{
            size:4,
            numItems:100
        });
        this.petal.addInstancedAttribute("iScale",scale,{
            size:1
        });


    }
}