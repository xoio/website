import Vue from 'vue'
import events from './events'
import chibi from 'chibi-dom'
import HeaderComponent from './components/masthead'
import PageComponent from './components/page'
import GLController from './gl/GLController'
import viewer from './components/viewer'
import {HTMLRouter,EVENT_ROUTE_CHANGED} from "@thi.ng/router";



/**
 * A tightly packed class that deals with everything on the site.
 */
class Controller {
    vue:Vue;
    system:any;
    gl:GLController;
    scroller:any;
    isAnimating:boolean = false;

    mouseX:number = 0;
    mouseY:number = 0;
    // reference to styling of the main element that holds everything.
    canvas:any = window.getComputedStyle(document.querySelector("#APP"));

    // array of all global assets needed for the site.
    assets:Array<any> = [];

    header:HeaderComponent = new HeaderComponent();
    page:PageComponent = new PageComponent();



    constructor() {

        // initialize a new scroll helpers
        this.scroller = new chibi.ScrollTracker();

        // setup initial data about the current system.
        this.system = {
            mobile:chibi.isMobile(),
            webgl:chibi.hasWebGL()
        };

        return this;
    }

    onMount(vue){

        // if we can run webgl - start things up.
        if(this.system.webgl){
            this.gl = new GLController(vue);


            // TODO run some more mobile checks to ensure a good experience,
            // otherwise run GL
            if(this.system.mobile){
               // this.gl.run();
            }else {
                this.gl.run();
            }

        }else {
            // TODO come up with something for non webgl folk.
        }
    }

    start(content,assets:Array<any>){

        let controller = this;
        this.assets = [...assets];


        this.vue = new Vue({
            el:"#APP",
            data:{
                title:"xoio",

                // reference to the last event tha twas fired.
                lastEvent:"",

                // router for the site.
                router:Controller._setupRouter(),

                // any additional assets besides project content
                assets:this.assets,

                // content list of all the projects for the site
                content:content,

                // number of projects
                numProjects:0,

                // resolution of the container.
                resolution:[],

                // thumbnail resolution
                thumbnailResolution:[1024,768],

                // scroll helper
                scroller: new chibi.ScrollTracker(),

                // flag to indicate whether or not this is a mobile device or not
                isMobile:this.system.mobile,

                // determines if there is webgl or not
                hasWebGL:this.system.webgl,

                // keeps track of what project we're looking at on the main page.
                projectIndex:0,

                // index for the next project in the list
                nextProjectIndex:1,

                // index for the previous project in the list.
                previousProjectIndex:0,

                // check to help figure out if we're in the middle of an animation or not
                isAnimating:false,

                // when on the home page, this stores the current details that need to be shown
                // as well as serve as the lookup of the project to show in the viewer.
                currentProject:{
                    projectId:0,
                    title:"",
                    copy:"",
                    assets:[],
                },

                // whether or not the viewer is currently showing.
                viewerShowing:false
            },

            mounted(){
                let containerStyle = getComputedStyle(document.querySelector("#APP"));

                this.controller = controller;
                document.body.classList.add("on-load");
                controller.onMount(this);

                // get a count of the number of projects
                this._self.numProjects = Object.keys(this._self.content).length;

                // setup indices
                this.resolution[0] = parseInt(containerStyle.width);
                this.resolution[1] = parseInt(containerStyle.height);

                // emit the mounted event to all components
                this._self.$emit(events.MOUNTED);

                window.addEventListener('resize',() => {

                    // update canvas dimensions if desktop
                    if(controller.system.mobile){
                        controller.canvas = containerStyle;
                    }
                    this.resolution[0] = parseInt(containerStyle.width);
                    this.resolution[1] = parseInt(containerStyle.height);

                    this.$emit(events.RESIZE);
                });

                // if not mobile, add mousemove refeerence
                if(!controller.system.mobile){
                    window.addEventListener("mousemove", e =>{

                        let canvasHalfWidth = parseInt(controller.canvas.width) / 2;
                        let canvasHalfHeight = parseInt(controller.canvas.height) / 2;

                        controller.mouseX = ( e.clientX - canvasHalfWidth) / 2;
                        controller.mouseY = ( e.clientY - canvasHalfHeight) / 2;

                        this.$emit(events.MOUSE_MOVE,controller.mouseX,controller.mouseY);
                    });
                }

               // let all components know that the route has changed.
               this.router.addListener(EVENT_ROUTE_CHANGED, e => {
                   this._self.$emit(EVENT_ROUTE_CHANGED,e);

                   // TODO there is probably a better way of doing this that escapes me at the moment.
                   controller.onRouteChange(e,this,controller);
               });

               // navigate to the currently requested route
               this.router.routeTo(window.location.pathname);

            },
            updated(){

                // delay emitting this event a bit - can still be a bit fast.
                setTimeout(()=>{
                    this._self.$emit(events.DOM_UPDATE);
                })
            },
            render(h){

                return h("main",{
                    domProps:{
                        id:'APP'
                    },
                    props:this._self._data,

                },[

                    // Global components
                    controller.header.render(h,this),
                    controller.page.render(h,this._self),
                    viewer.render(h,this._self)

                ])
            },

        });

    }

    /**
     * Sets current project index to the next project
     * Remember that this will also affect GL grid mesh as well.
     *
     * Note that this should be disabled when not on the home page.
     * @private
     */
    _next(vue){
        let idx = vue["projectIndex"];
        let len = vue["numProjects"];

        if(idx < len - 1){
            vue["projectIndex"] += 1;
        }else if(idx === len - 1){
            vue["projectIndex"] = 0;
        }

        // figure out what the next index would be
        if(vue["projectIndex"] < len - 1){
            vue["nextProjectIndex"] = vue["projectIndex"] + 1;
        }else if(vue["nextProjectIndex"] === len - 1){
            vue["nextProjectIndex"] = 0;
        }

        console.log("next called, index is now ",vue["projectIndex"], "next is ", vue["nextProjectIndex"]);
        vue.$emit(events.HOME_INDEX_CHANGED);
    }

    /**
     * Sets current project index to the previous index.
     * Remember that this will also affect GL grid mesh too.
     * Note that this should be disabled when not on the home page.
     * @private
     */
    _previous(vue){
        let idx = vue["projectIndex"];
        let len = vue["numProjects"];

        if(idx > 0){
            vue["projectIndex"] -= 1;
        }else {
            vue["projectIndex"] = len - 1;
        }

        // figure out what the next index would be
        if(vue["projectIndex"] > 0) {
            vue["previousProjectIndex"] = vue["projectIndex"] - 1;
        }else if(vue["previousProjectIndex"] < 0){
            vue["previousProjectIndex"] = len - 1;
        }

        console.log("previous called, index is now ",vue["projectIndex"],"\n")
        vue.$emit(events.HOME_INDEX_CHANGED);
    }

    /**
     * Setup routing for everything.
     * @private
     */
    static _setupRouter(){
        return new HTMLRouter({
            useFragment:true,
            defaultRouteID:"home",
            routes:[
                {
                    id:"home",
                    title:"Xoio",
                    match:[]
                },
                {
                    id:"project",
                    title:"Project",
                    match:["project","?name"]
                }
            ]
        });
    }

    /**
     * Handles scrolling event - done here to make it easier to disable if need be.
     * @param e {Object} the event from the "wheel" listener.
     * @private
     */
    _onScroll(e){

        let direction = this.scroller.getWheelDirection(e);
        let speed = this.scroller.checkSpeed(e);

        if(speed >= 0.5 && this.isAnimating === false){

            // first allow any pre-animations that need to happen happen
            // @ts-ignore
            this.$emit(events.HOME_PREANIM);

            // listen for when that pre-animation is complete
            // @ts-ignore
            this.$on(events.HOME_PREANIM_COMPLETE, e =>{
                if(!this.isAnimating){
                    // first animate things out, change index, then animate in.
                    this.isAnimating =  true;

                    let controller = this["controller"]

                    switch (direction) {
                        case "up":
                            controller._previous(this);
                            break;

                        case "down":
                            controller._next(this);
                            break;
                    }
                }
            });

        }



        // emit global event passing in scroll props like speed and direction(TODO)
        // @ts-ignore
        this.$emit(events.SCROLL,{
            event:e,
            direction:this.scroller.getWheelDirection(e),
            speed:this.scroller.checkSpeed(e)
        });
    }

    /**
     * Callback that gets run when the router receives a route change request
     * @param e {Object} the event object
     * @param vue {Object} A reference to the Vue instance
     * @param controller {Object} a reference to this same controller object.
     */
    onRouteChange(e,vue,controller){
        let route = e.value.id;
        if(route === "home"){

            // if on desktop - set up wheel event
            if(!vue.isMobile){
                window.addEventListener("wheel",controller._onScroll.bind(vue),{passive:true})
            }

        }else{
            window.removeEventListener("wheel",controller._onScroll);
        }
    }

}

export default Controller;