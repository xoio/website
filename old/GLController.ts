import createRenderer from './jirachi/core/gl'
import createFBO from './jirachi/core/fbo'
import createShader from './jirachi/core/shader'
import createTexture from './jirachi/core/texture'
import quad from './jirachi/geometry/quad'
import {PerspectiveCamera} from './jirachi/framework/camera'

import Mesh from './jirachi/framework/mesh'
import events from "../events";
import ImageFlipper from "./ImageFlipper";
import Loader from "./AssetLoaderr";
import ParticleSystem from "./ParticleSystem";
import {TextureFormat,ShaderFormat} from "./jirachi/core/formats";

import renderQuadVert from './jirachi/shaders/plane.vert'
import renderQuadFrag from './jirachi/shaders/plane.frag'
import Quad from "./jirachi/geometry/quad";

/**
 * Manages all WebGL aspects
 */
class GLController {

    camera:any;
    vue:any;
    gl:any;
    resolution:[number,number] = [0,0];
    flipper:ImageFlipper;

    // stuff for the particle system
    systemFBO:any;
    system:ParticleSystem;

    bgFBO:any;

    renderQuad:any;
    renderQuadShader:any;

    thumbnails:Array<any> = [];

    // all assets for every project
    assets:Array<any>;

    constructor(vue) {

        this.vue = vue;

        // build a WebGL renderer.
        this.gl = createRenderer();

        // setup the camera.
        this._setupCamera();

        // setup the general components needed for the WebGL stuff
        this.setup();

        // setup the particle system
        this._setupParticleSystem();

        // setup the main scene elements.
        this._setupScene();

        // builds the rendering plane.
        this._buildRenderPlane();

        // when DOM is mounted, set up GL stuff
        vue.$on(events.MOUNTED, e =>{
            this.setup();
            this._setupCamera();
           // this._setupFlipper();
        });

        // fire off certain things when the page is re-sized.
        vue.$on(events.RESIZE, e =>{
            this.resizeContext();
            this.camera.resize(this.gl.canvas.width / this.gl.canvas.height);
        })

        // when the thumbnails are loaded, process them into textures.
        vue.$on(events.THUMBNAILS_LOADED,this._processThumbnails.bind(this));

    }

    /**
     * Processes thumbnail images and turns them into textures.
     * @private
     */
    _processThumbnails(){
        let thumbs = this.vue["thumbnails"];
        thumbs.forEach(thumb => {
            // build texture
            this.thumbnails.push(createTexture(this.gl,new TextureFormat(this.gl,{
                data:thumb
            })));
        })
    }

    /**
     * Resize GL context according to container.
     * TODO maybe debounce a bit
     */
    resizeContext(){
        let canvas = this.gl.canvas;
        let node = canvas.parentElement;
        let styles = getComputedStyle(node);

        canvas.width = parseInt(styles.width);
        canvas.height = parseInt(styles.height);

        this.resolution[0] = canvas.width;
        this.resolution[1] = canvas.height;
        this.camera.resize(this.gl.canvas.width / this.gl.canvas.height);
        //this.gridCamera.resize(this.gl.canvas.width / this.gl.canvas.height);
    }

    setup(){
        // append canvas to DOM
        let el = document.querySelector("#APP");
        el.appendChild(this.gl.canvas);
        this.resizeContext();

    }

    _setupCamera(){
        // setup camera
        this.camera = new PerspectiveCamera({
            near:0.1,
            far:10000.0,
            aspect: this.gl.canvas.width / this.gl.canvas.height,
        });

        //this.gridCamera = new PerspectiveCamera({
        //    fov:60,
        //    near:0.1,
        //    far:1000.0,
        //    aspect: this.gl.canvas.width / this.gl.canvas.height,
        //});

        this.camera.setZoom(-50)
        //this.gridCamera.setZoom(-40);

    }

    _setupFlipper(){
        let gl = this.gl;
        let vue = this.vue;
        let content = vue.content;

        let thumbs = [];

        // grab thumbnails out of content assets and feed to ImageFlipper
        for(let i in content){
            let proj = content[i].assets;
            proj.forEach(thumb =>{

                let segments = thumb.src.split("/");
                let name = segments[segments.length - 1];

                if(name.search("thumb") !== -1 ){
                    thumbs.push(thumb);
                }
            })
        }

        // thumbnails should have same width / height
        let resolution = [thumbs[0].width,thumbs[0].height]


        this.flipper = new ImageFlipper(this.gl,this.vue,[thumbs[0].width,thumbs[0].height],thumbs);
    }
    run(){

        let gl = this.gl;

        let rgb = [
            1.0 / 37.0,
            1.0 / 37.0,
            1.0 / 41.0
        ]
        let animate = () => {
            requestAnimationFrame(animate);
            gl.clearScreen(rgb[0],rgb[1],rgb[2]);



        };

        animate();

    }

    /**
     * Constructs the rendering plane for rendering stuff from FBOs onto. Defaults to full screen.
     * @private
     */
    _buildRenderPlane(){
        this.renderQuadShader = createShader(this.gl,new ShaderFormat(renderQuadVert,renderQuadFrag));
        this.renderQuad = new Mesh(this.renderQuadShader,new Quad());
    }

    // --------------------- SETUP SCENE -------------------- //
    _setupScene(){
        let width = this.vue.controller.resolution[0];
        let height = this.vue.controller.resolution[1];

        this.bgFBO = createFBO(this.gl,new TextureFormat(this.gl,{
            width:width,
            height:height
        }));

        
        this.vue.on(events.RESIZE,(w,h) =>{
            this.bgFBO.resize(w,h);
        })
    }

    // --------------------- PARTICLE SYSTEM STUFF --------------------------------- //

    _renderParticleSystem(){
        // render particle system
        this.systemFBO.bind();
        this.system.draw(this.camera);
        this.systemFBO.unbind();
    }

    _setupParticleSystem(){
        let vue = this.vue;
        let width = this.vue.controller.resolution[0];
        let height = this.vue.controller.resolution[1];

        this.system = new ParticleSystem(this.gl,vue.assets[0].data,vue.resolution,5000);
        this.systemFBO = createFBO(this.gl,new TextureFormat(this.gl,{
            width:width,
            height:height
        }));

    }
}

export default GLController;