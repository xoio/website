import {Component} from "../interfaces";
import {charSplitHiccup, convertTemplate, idToTitle, setHiddenTitle} from "./component-helpers";
import {EVENT_ROUTE_CHANGED} from "@thi.ng/router";
import events from "../events";
import chibi from 'chibi-dom'
let count = 0;
/**
 * Defines the main page where users can look through all the projects.
 */
export default class Page implements Component {

    vue:any;
    scroller:any;

    mounted(){
        let vue = this.vue;


        // while on the home page - this gets triggered if we're about to shift to a new item.
        vue.$on(events.HOME_PREANIM, e =>{
            this._hideTitle();
        });

        // while on the home page - this gets triggered after we've changed to the next item
        vue.$on(events.DOM_UPDATE, e =>{

        });

        vue.$on(events.HOME_INDEX_CHANGED, e =>{

            setTimeout(()=>{
                this._showTitle();
            },800)
        });

        //vue.$on(events.PRE_TRANSITION_PROJECT,this._hideTitle);
        let nodes = chibi.getNodes(".project-title-section");
        if(nodes  !== null){
            this.adjustContainer();
            this.vue.$on(events.RESIZE,this.adjustContainer.bind(this));
        }
    }
    tree(vue){
        // build scroller
        this.scroller = new chibi.ScrollTracker();

        let content = this._buildProjectStructure(vue);

        // setup routing
        this.vue.$on(EVENT_ROUTE_CHANGED,this.routeChange.bind(this));

        // build some stuff out when things are mounted.
        this.vue.$on(events.MOUNTED,this.mounted.bind(this));

        return [
            [
                "section#home-content-wrap",
                content[this.vue.projectIndex]
            ]
        ]
    }

    render(h,vue){
        this.vue = vue;
        return convertTemplate(h,this.tree(vue),{
            domProps:{
                id:"home-content"
            }
        });
    }
    /**
     * Ensures that the container stays the same dimensions
     */
    adjustContainer(){
        let nodes = chibi.getNodes(".project-title-section");

        nodes.forEach(node =>  {
            node.style.width = this.vue.resolution[0] + "px";
            node.style.height = this.vue.resolution[1] + "px";
        });

    }

    /**
     * Triggered when the route changes
     */
    routeChange(e){

        let details = e.value;


        /**
         * if it's not the home page then we need to bring up the viewer and load the correct project.
         * if the viewer is already up then all we need to do is to remove the viewer and unload the content.
         */
        if(details.id !== "home"){
            //remove all current event listeners fo rhte page.
            this.removeListeners();

            // todo animate things out

        }else{


            this._showTitle();
        }

    }

    /**
     * Remove homepage elements from the screen
     */
    removeElements(){

    }
    removeListeners(){
        let nodes = chibi.getNodes(".project-title-section h3");
        nodes.forEach(node =>{
            node.removeEventListener("click",this.click.bind(this));
        });
    }

    /**
     * Add all necessary listeners when we're on the homepage.
     */
    addListeners(){
        let nodes = chibi.getNodes(".project-title-section h3");
        nodes.forEach(node =>{
            node.addEventListener("click",this.click.bind(this));
        });
    }

    /**
     * Builds out the html structure for showing projects.
     * @param vue {Object} the vue instance
     * @private
     */
    _buildProjectStructure(vue){
        // build out project list
        let content = [];
        for(let i in vue.content){
            let project = vue.content[i];
            let title = project.meta.title;

            // generate a title based on the ID if the title isn't specified in the metadata
            if(title === undefined){
                title = idToTitle(project.meta.id);
            }

            // remember that title is hidden at first
            content.push([
                `section#${project.meta.id}.project-title-section`,
                ["h3",{
                    class:project.meta.id + " home-title"
                },setHiddenTitle(charSplitHiccup(title))]
            ]);
        }
        return content;
    }

    /**
     * Hide the project title.
     * @private
     */
    _hideTitle(){

        // temporarily halt listeners.
        this.removeListeners();

        // animate title in
        let title = chibi.getNodes(".home-title span")
        let count = title.length;
        let animated = 0;
        let onTransitionEnd = () =>{
            animated += 1;

        };
        title.forEach(chunk => {
            let time = Math.random() * 1000;
            setTimeout(() =>{
                chunk.classList.add("hide-up");
                chunk.classList.add("hide-down");
            },time);



            // TODO do we need to remove event listener since section is
            // basically destroyed and recreated with new data each time?
            chunk.addEventListener("transitionend",onTransitionEnd);
        });



        let timer = setInterval(()=>{
            if(animated === count){
                clearInterval(timer);
                title.forEach(chunk => {
                    chunk.removeEventListener("transitionend",onTransitionEnd);
                });

                // re-add event listeners
                this.addListeners();
                this.vue.$emit(events.HOME_PREANIM_COMPLETE);
            }
        })
    }

    /**
     * Show project title
     * @private
     */
    _showTitle(){

        // temporarily halt listeners
        this.removeListeners();

        // animate title in
        this.adjustContainer()

        let title = chibi.getNodes(".char")
        let count = title.length;
        let animated = 0;
        let onTransitionEnd = () =>{
            animated += 1;
        }


        title.forEach(chunk => {

          //  console.log("removing ",chunk)
            chunk.classList.remove("hide-up");
            chunk.classList.remove("hide-down");


            // TODO do we need to remove event listener since section is
            // basically destroyed and recreated with new data each time?
            chunk.addEventListener("transitionend",onTransitionEnd);
        })


        let timer = setInterval(()=>{
            if(animated === count){
                title.forEach(chunk => {
                    // TODO do we need to remove event listener since section is
                    // basically destroyed and recreated with new data each time?
                    chunk.removeEventListener("transitionend",onTransitionEnd);
                });
                clearInterval(timer);
            }
        })
        setTimeout(()=>{
            this.vue['isAnimating'] = false;

            // re-enable listeners
            this.addListeners();

        },1000)
    }


    /**
     * Deals with things when they are clicked on.
     * @param e
     */
    click(e){
        let target = e.target;
        let project = null;

        switch(target.tagName){

            case "SPAN":
                project = e.target.parentElement.parentElement.id;
                break;

            default:
                project = e.target.parentElement.id;
                break;
        }

        // hide the current title.
        this._hideTitle();

        // trigger all events that need to happen before we move onto the project
        // view.
        this.vue.$emit(events.TRANSITION_PROJECT,project);
    }

}