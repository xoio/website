import {convertTemplate, charSplit, idToTitle, lookupProject} from './component-helpers'
import {EVENT_ROUTE_CHANGED} from "@thi.ng/router";
import events from '../events'

/**
 * This component builds out the project viewer
 */
export default {

    //whether or not the viewer is currently visibile.
    isVisible:false,

    tree(vue) {

        let project = vue.currentProject;
        if(project.assets.length > 0){
            let assets = project.assets.map(asset =>{
                return [
                    "div.project-picture",
                    ["img",{src:asset}]
                ]
            });

            vue.$on(EVENT_ROUTE_CHANGED,this.routeChange.bind(this));

            return [
                "div.project-wrap",
                ["div.close-button"],
                ["h1",project.meta.title],
                ["div.copy",project.copy],
                ["div.media", ...assets]
            ]
        }
    },

    /**
     * Triggered when the route changes
     */
    routeChange(e){

        let details = e.value;

        /**
         * if it's not the home page then we need to bring up the viewer and load the correct project.
         * if the viewer is already up then all we need to do is to remove the viewer and unload the content.
         */
        if(details.id !== "home"){

            // set the project to show in the viewer
           let newProj = details.params.name;
           this.vue.currentProject = this.vue.content[newProj];


        }

    },

    render(h,vue){
        this.vue = vue;
        let _class;

        if(vue.viewerShowing){
            _class = "show-viewer";
        }else{
            _class = "";
        }

        // when we're transitioning to showing a project - make sure to change
        // the flag to show the viewer
        vue.$on(events.TRANSITION_PROJECT,(id) =>{
            vue.viewerShowing = true;
            vue.currentProject = lookupProject(vue.content,id);
        });

        return convertTemplate(h,this.tree(vue),{
            domProps:{
                id:"project-viewer",
                className:_class
            }
        });
    }
}