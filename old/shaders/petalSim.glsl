

/**
Transform feedback simulation for moving particle system across the screen
*/

in vec4 position;
in vec3 velocity;
in vec3 origin;

out vec4 oPos;
out vec3 oVel;
void main(){
    oPos = position;
    oVel = velocity;

    // alpha holds life , when less than 0, reset ps
    if(oPos.a < 0.0){
        oPos.a = 1.0;
        oPos.xyz = origin.xyz;
    }else {

        vec3 curl = curlNoise( oPos.xyz * 0.1 );
        oVel += curl * 0.01;
        oVel *= 0.97;
        oVel.x += 0.002;

        oPos.xyz += oVel;
        oPos.a -= 0.0005;
    }

}