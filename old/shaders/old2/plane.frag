precision highp float;

uniform sampler2D currentTexture;
uniform sampler2D nextTexture;
uniform sampler2D transitionTexture;
uniform float mixRatio;
uniform float threshold;


in vec4 vCol;
in vec2 vUv;
out vec4 glFragColor;


void main(){
    vec4 curr =  texture(currentTexture,vec2(vUv.x,1.0 - vUv.y));
    vec4 next =  texture(nextTexture,vec2(vUv.x,1.0 - vUv.y));
    vec4 transitionTexel = texture(transitionTexture,vUv);
    float r = mixRatio * (1.0 + threshold * 2.0) - threshold;
    float mixf=clamp((transitionTexel.r - r)*(1.0/threshold), 0.0, 1.0);
    //glFragColor = mix(curr,next,mixf);
    glFragColor = vec4(1.0,1.0,0.0,1.0);

}