uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

in vec2 position;

out vec2 vUv;
void main(){
    vUv = vec2(0.0,1.0) + vec2(0.5,-0.5) * (position + 1.0);
    gl_Position = vec4(position,0.0,1.);
}
