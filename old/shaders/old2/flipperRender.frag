precision highp float;

uniform sampler2D uTex0;
in vec2 vUv;
out vec4 glFragColor;

void main(){
    glFragColor = texture(uTex0,vUv);
}