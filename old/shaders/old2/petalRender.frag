precision highp float;
uniform vec2 resolution;
uniform sampler2D uTex0;
uniform float time;

out vec4 glFragColor;
in float vAlpha;

const vec3 baseColor = vec3(212.0,32.0,133.0);

void main() {
    vec2 uv = gl_FragCoord.xy / resolution;
    vec3 color = normalize(baseColor);

    vec4 dat = texture(uTex0,vec2(uv.x,1.0 -uv.y));

    // Time varying pixel color
    vec3 col = 0.5 + 0.5*sin(color * time+uv.xyx+vec3(1,0,2));
    col = mix(color,col,0.8) + 0.2;

    dat*=dat;

    glFragColor = dat * vec4(col,1.);
    //glFragColor.a = vAlpha;
    glFragColor.a = mix(1.0,0.0,vAlpha);
}
