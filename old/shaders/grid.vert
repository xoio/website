uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 modelMatrix;

in vec3 position;
in vec2 uv;

out vec2 vUv;
void main() {
    vec3 pos = position;
	vUv = uv;
	gl_Position = projectionMatrix * modelViewMatrix * modelMatrix * vec4(pos,1.);
}
