precision highp float;
uniform vec2 resolution;
uniform float time;

out vec4 glFragColor;
in float vAlpha;

const vec3 baseColor = vec3(212.0,32.0,133.0);

void main() {
    vec2 uv = gl_FragCoord.xy / resolution;
    vec3 color = normalize(baseColor);

    // Time varying pixel color
    vec3 col = 0.5 + 0.5*sin(color * time+uv.xyx+vec3(1,0,2));
    col = mix(color,col,0.8) + 0.2;


	glFragColor = vec4(col, vAlpha);
}
