import Mesh from './jirachi/framework/mesh'
import createShader from './jirachi/core/shader'
import createTexture from './jirachi/core/texture'

import vert from './shaders/grid.vert'
import frag from './shaders/grid.frag'
import {ShaderFormat, TextureFormat} from "./jirachi/core/formats";
import {Vue} from "vue/types/vue";
import Plane from "./jirachi/geometry/plane";


export default class GridMesh{
    // resolution of each part of the grid
    resolution:[number,number];

    // overall size of the grid.
    gridSize:[number,number];

    gl:any;

    vue:Vue;

    // thumbnails to show
    thumbnails:Array<any> = [];

    //meshes to build the grid.
    meshes:Array<any> = [];

    index:number = 0;

    isAnimating:boolean = false;
    constructor(gl,vue,{

        xsize=20,
        ysize=10,

        gridWidth=480,
        gridHeight=240
    }={}){

        this.gl = gl;
        this.resolution = [gridWidth,gridHeight];
        this.gridSize = [xsize,ysize];

        this.vue = vue;

        // note that current index handling will be taken care of by the main page controller.

        window.addEventListener('keydown',(e) =>{
            console.log(e.keyCode)

            if(e.keyCode === 87){
                this.animateOut();
            }else{
                this.animateIn()
            }
        })
        this._loadAssets();
        this._buildGrid();
    }

    draw(camera){
        let currentIndex = this.vue["projectIndex"];
        let nextIndex = 0;

        this.gl.enableAlphaBlending();

        if(nextIndex < this.thumbnails.length){
            nextIndex = currentIndex + 1;
        }

        if(nextIndex >= this.thumbnails.length){
            nextIndex = 0;
        }


        if(this.thumbnails.length > 0) {
            this.thumbnails[currentIndex].bind(0);
            this.thumbnails[nextIndex].bind(1);

            this.meshes.forEach(mesh => {
                let pos = mesh["position"];
                mesh.draw(camera);
                mesh.translate([pos.x,pos.y,pos.z]);

                mesh.shader.float("alpha",mesh["alpha"])
                mesh.shader.int("currentTex",0);
               // mesh.shader.int("nextTex",1);
            })
        }

    }

    /**
     * Animate all of the meshes in.
     */
    animateIn(){
        this.meshes.forEach(mesh => {
            window["TweenMax"].to(mesh.position, 1.5, {
                //x: mesh.origPosition.x,
                //y: mesh.origPosition.y,
                z:mesh.origPosition.z,
                ease:window["Elastic"].easeIn,
                delay: Math.random()
            });

            // animate mesh alpha. When 0, shift tiles to opposite end.
            window["TweenMax"].to(mesh,1.0,{
                alpha:1,
                ease:window["TweenMax"].easeInOut,
                delay:Math.random()
            });
        });
    }

    /**
     * Animates all the grid pieces out of frame
     */
    animateOut(){
        this.meshes.forEach(mesh => {
            window["TweenMax"].to(mesh.position,1,{
                z:-5,
                ease:window["Elastic"].easeIn,
                delay:Math.random()
            });

            // animate mesh alpha. When 0, shift tiles to opposite end.
            window["TweenMax"].to(mesh,0.5,{
                alpha:0,
                ease:window["TweenMax"].easeInOut,
                delay:Math.random() + 1
            });
        })
    }

    _buildGrid(){

        // ================ BUILD INSTANCED DATA ================== //

        let xsize = this.resolution[0] / this.gridSize[0];
        let ysize = this.resolution[1] / this.gridSize[1];

        let xgrid = this.gridSize[0];
        let ygrid = this.gridSize[1];

        let plane = new Plane(xsize,ysize,1,1);
        let shader = createShader(this.gl,new ShaderFormat()
            .vertexSource(vert)
            .fragmentSource(frag));

        let uvs = plane.uvs;
        let vertices = plane.vertices;


        for ( let i = 0; i < xgrid; i ++ ) {
            for ( let j = 0; j < ygrid; j ++ ) {
                let ox = i;
                let oy = j;
                let ux = 1 / this.gridSize[0];
                let uy = 1 / this.gridSize[1];

                let x = ( i - xgrid / 2 ) * xsize;
                let y = ( j - ygrid / 2 ) * ysize;

                // make sure each position is centered
                x += xsize / 2;
                y += ysize / 2;

                let mesh = new Mesh(shader);
                mesh.setPrimitiveMode(this.gl.TRIANGLE_STRIP);

                mesh.addAttribute("position",plane.vertices);
                mesh.addAttribute("uv",this._change_uvs([...plane.uvs],ux,uy,ox,oy),{
                    size:2
                });

                mesh["position"] = {
                    x:x,
                    y:y,
                    z:0
                };
                mesh["origPosition"] = {
                    x:x,
                    y:y,
                    z:0
                },
                mesh["alpha"] = 1.0;
                mesh["sphere"] = {
                    phi:Math.random(),
                    theta:Math.random(),
                    phiSpeed:Math.random() * 0.001,
                    thetaSpeed:Math.random() * 0.001
                };
                this.meshes.push(mesh);
            }
        }

    }

    _change_uvs( uvs, unitx, unity, offsetx, offsety ) {
        for ( let i = 0; i < uvs.length; i += 2 ) {
            uvs[ i ] = ( uvs[ i ] + offsetx ) * unitx;
            uvs[ i + 1 ] = ( uvs[ i + 1 ] + offsety ) * unity;
        }
        return uvs;
    }


    /**
     * Load thumbnail assets for preview.
     * @private
     */
    _loadAssets(){
        let content = this.vue["content"];
        let count = 0;
        let urls = [];
        for(let proj in content){
            urls.push({
                id:proj,
                url:content[proj]["thumb"]
            });
            count += 1;
        }

        urls.forEach(proj => {
            let img = new Image();
            img.src = proj.url;
            img.onload = () =>{

                let tex = createTexture(this.gl,new TextureFormat(this.gl).setWidth(img.width)
                    .setHeight(img.height));
                tex.loadImage(img);
                tex.id = proj.id


                this.thumbnails.push(tex)
            }
        });

        let timer = setInterval(() =>{

            if(this.thumbnails.length > 0 && this.thumbnails.length === count){
                // re-arrange things so that the thumbnail order matches the project order
                let fin = [];
                for(let proj in content){
                    this.thumbnails.forEach(thumb => {
                        if(thumb.id === proj){
                            fin.push(thumb);
                        }
                    });
                }
                this.thumbnails = fin;

                clearInterval(timer);
            }
        })
    }


    tweenPositions(){
        let meshes = this.meshes;
        let tweenMax = window.TweenMax;
        meshes.forEach(mesh => {

            tweenMax.to(mesh,{

            });
        })
    }

    static  _getScrollDirection(e){
        if(e.deltaY < 0){
            return "up"
        }else {
            return "down";
        }
    }
}

