/**
 * Returns the direction of a mouse scroll.
 * @param e
 */
export function getDirection(e){
    if(e.detail > 0 || e.wheelDelta > 0){
        return "down";
    }else if(e.detail < 0 || e.wheelDelta < 0){
        return "up"
    }
}

export function processWheelEvent(e,options={
    isChrome:false,
    maxForce:7
}){
    let delta = e.deltaY;
    let direction = "";

    if(delta > 0){
        direction =  "down";
    }else if(delta < 0){
        direction =  "up"
    }

    // divide delta by 100 to try and normalize.
    if(delta !== undefined && delta.toString().split("") > 2){
        delta /= 100;
    }

    // just solve for whether or not something needs to happen on a fast enough mouse wheel event here
    let triggered = false

    // max value delta has to hit before we've "scrolled enough"
    let max = options.maxForce

    // if chrome, delta is in the 100s, things need to be tweaked a bit.
    if(options.isChrome){
        delta /= 100.0;
        max = 2.5;
    }


    if((delta > 0 && delta > max) || (delta < 0 && delta < max * -1)){
        triggered = true;
    }

    return {
        triggered:triggered,
        direction:direction
    }
}