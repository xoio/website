import Vue from "vue"
import chibi from 'chibi-dom'
import events from "./events";
import {processWheelEvent} from './mouseUtils'
import Page from './components/page'
import GLController from './gl/GLController'
import Menu from './components/menu'
export default class Controller {

    // content
    content:Object;

    // assets
    globalAssets:Array<any>;

    // reference to the Vue instance
    vue:Vue;

    // canvas of the app
    canvas:any;

    // resolution of the viewing area
    resolution:Array<any> = [];

    // mouse position
    mouseX:number = 0;
    mouseY:number = 0;

    // contains system information
    systemInformation:any;

    // whether or not the user's computer has webgl support
    hasWebGL:boolean = false;

    // the page component
    page:Page = new Page();

    // the menu component
    menu:Menu = new Menu();

    // manages the WebGL parts
    glScene:GLController;

    // whether or not we've loaded the thumbnails
    thumbnailsLoaded:boolean = false;


    // ========= helpers to handler resizing events =============== //
    resizing:boolean = false;
    scrolling:boolean = false;
    resizeDelta:number = 200;
    rtime:any;


    /**
     * Kicks everything off
     * @param content {Object} list of all assets necessary to show projects
     * @param globalAssets {Array} list of global assets
     */
    start(content,globalAssets) {

        this.globalAssets = globalAssets;
        this.content = content;


        // get system information
        // setup initial data about the current system.
        this.systemInformation = {
            mobile:chibi.isMobile(),
            chrome:chibi.isChrome(),
            webgl:chibi.hasWebGL()
        };

        // check to see if we can run webgl
        if(this.systemInformation.webgl) {
            this.hasWebGL = true;
        }

        // construct the vue object.
        this._buildVue();

        // hide loading panel once the thumbnails are loaded
        this.vue.$on(events.THUMBNAILS_LOADED,() => {
            let el = document.querySelector("#load-panel");
            el.classList.add("on-load");
        })
    }

    /**
     * Setup all listeners.
     * @private
     */
    _setupEvents(){
        let containerStyle = this._getContainerStyle();

        // set initial window resolution
        this.resolution[0] = parseInt(containerStyle.width);
        this.resolution[1] = parseInt(containerStyle.height);
        this.canvas =  window.getComputedStyle(document.querySelector("#APP"));

        // set window resolution during resize events.
        window.addEventListener('resize',e => {
            this.resolution[0] = parseInt(containerStyle.width);
            this.resolution[1] = parseInt(containerStyle.height);
            this.canvas =  window.getComputedStyle(document.querySelector("#APP"));

            // let other sections of the app know that a resize event was triggered.
            this.vue.$emit(events.RESIZE,this.resolution[0],this.resolution[1]);

            // setup system to allow components to know when the resize event is complete.
            this.rtime = new Date();
            if(!this.resizing){
                this.resizing = true;

                let timer = setInterval(()=>{
                    let current = new Date();
                    // @ts-ignore
                    let diff = current - this.rtime;

                    if(diff > this.resizeDelta){

                        this.vue.$emit(events.RESIZE_COMPLETE);
                        this.resizing = false;
                        clearInterval(timer);
                    }
                })
            }
        });



        // if not on mobile, keep track of mouse x and y
        if(!this.systemInformation.mobile){

            window.addEventListener('keydown', e => {
                this.vue.$emit(events.KEYDOWN,e);
            });

            window.addEventListener('mousedown',() =>{
                this.vue.$emit(events.MOUSE_DOWN);
            })
            window.addEventListener('mouseup',() =>{
                this.vue.$emit(events.MOUSE_UP);
            });

            window.addEventListener('mousemove',e => {

                let canvasHalfWidth = parseInt(this.canvas.width) / 2;
                let canvasHalfHeight = parseInt(this.canvas.height) / 2;



                this.mouseX = ( e.clientX - canvasHalfWidth) ;
                this.mouseY = ( e.clientY - canvasHalfHeight) * 0.3;

                // emit mouse move event.
                this.vue.$emit(events.MOUSE_MOVE,this.mouseX,this.mouseY);
            });

            // add wheel event
            // firefox will listen for either wheel or DOMMouseScroll, separate things out
            // because according to MDN, wheel event i
            window.addEventListener('wheel', this._onWheel.bind(this));

        }
    }

    /**
     * Handle mouse wheel events where necessary.
     * @param e
     * @private
     */
    _onWheel(e){
        let details = processWheelEvent(e,{
            isChrome:this.systemInformation.chrome,
            maxForce:5
        });
        this.rtime = new Date();
        if(!this.scrolling){
            this.scrolling = true;

            let timer = setInterval(()=>{
                let current = new Date();
                // @ts-ignore
                let diff = current - this.rtime;

                if(diff > this.resizeDelta){

                    this.vue.$emit(events.MOUSE_WHEEL_COMPLETE,details);
                    this.scrolling = false;
                    clearInterval(timer);
                }
            })
        }
        this.vue.$emit(events.MOUSE_WHEEL,details);
    }

    /**
     * Fired when vue is done mounting everything.
     * @private
     */
    _onMount(){

        // TODO not sure why a timer is needed - if no timer, then
        // things get triggered too early.
        let timer = setInterval(() =>{
            if(this.vue !== undefined && this.thumbnailsLoaded){
                this.vue.$emit(events.MOUNTED);

                // show the body
                document.body.classList.add("loaded");
                this._setupEvents();

                clearInterval(timer);
            }
        })
    }

    /**
     * Builds up the vue instance.
     * @private
     */
    _buildVue(){
        let controller = this;


        this.vue = new Vue({
            el: "#APP",
            data:{

                // reference for general website use.
                assets:this.globalAssets,

                // raw content datta
                content:this.content,

                // whether or not the menu is open. If it is we want to stop certain actions.
                menuOpen:false,

                // resolution of the main viewing area.
                resolution:controller.resolution,

                // reference to thumbnails for projects
                thumbnails:[],

                // reference to main controller object.
                controller:controller,

                // keeps track of what project we're looking at on the main page.
                projectIndex:0,

                // whether or not this client device is mobile or not.
                isMobile:this.systemInformation.mobile
            },

            mounted:controller._onMount.bind(this),

            render(h){
                return h("main",{
                    domProps:{
                        id:'APP'
                    },
                    props:this._self._data,

                },[


                    controller.page.render(h,this),
                    controller.menu.render(h,this)

                ])
            }
        });


        // if we're on a web gl capable system, then pass in the Vue instance.
        if(this.systemInformation.webgl && !this.systemInformation.mobile){

            this.glScene = new GLController(this.vue);
            this.glScene.setup();
            this.glScene.run();

        }

        // kickoff thumbnail loading
        this._loadThumbnails();
    }

    /**
     * Loads thumbnails to help ensure we have something to show the user.
     * @private
     */
    _loadThumbnails(){

        let thumbnails = []

        for(let i in this.vue["content"]){
            thumbnails.push(this.vue["content"][i].thumb);
        }
        let count = 0;
        let numThumbs = thumbnails.length;

        thumbnails.forEach(thumb => {
            let img = new Image();
            img.src = thumb;

            img.onload = () => {
                count += 1;
                this.vue["thumbnails"].push(img);
            }
        });

        // set a timer so we know when all the thumbnails have loaded.
        let timer = setInterval(() => {
            if(count === numThumbs){
                this.thumbnailsLoaded = true;
                this.vue.$emit(events.THUMBNAILS_LOADED);
                clearInterval(timer);
            }
        })
    }

    /**
     * Helper to get the container style
     * @private
     */
    _getContainerStyle(){
        return getComputedStyle(document.querySelector("#APP"));
    }



}