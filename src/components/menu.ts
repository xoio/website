import {Component} from "../interfaces";
import Vue from 'vue'
import {convertTemplate} from "./component-helpers";
import events from "../events";
import * as chibi from 'chibi-dom'
import randomColor from 'randomcolor'


/**
 * The main page that the user will see.
 */
export default class implements Component {

    vue:Vue;

    // width for each menu column
    columnWidth:number;

    // number of columns for the menu
    numColumns:number = 4;

    // whether or not the menu is currently open
    menuOpen:boolean = false;

    // color values for the gradient nav menu
    menuGradientColors:Array<any> = [];

    /**
     * Builds the html tree.
     */
    tree(){
        return [
            ["nav.menu",
                ["header",
                    ["h1",{id:"title",class:"cursor-pointer"},"xoio"]
                ],
                ["div#menu-button",""],
                ["ul.links",
                    ["li",
                    ["a",{href:"https://gitlab.com/xoio", target:"_blank"},"Gitlab"]],

                    ["li",
                        ["a",{href:"https://twitter.com/sortofsleepy",target:"_blank"},"Twitter"]],

                    ["li",
                        ["a", {href:"https://linkedin.com/in/sortofsleepy",target:"_blank"}, "LinkedIn"]
                    ]

                ]
            ]
        ]
    }

    /**
     * Renders the component
     * @param h
     * @param vue
     */
    render(h,vue){

        this.vue = vue;

        this.vue.$on(events.MOUNTED,this.mounted.bind(this));
        return convertTemplate(h,this.tree(),{
            domProps:{
                id:"menu"
            }
        });

    }

    onClick(e){
        let menuColumns = document.querySelector("#nav-columns");
        let menu = document.querySelector("#menu");
        /**
         * If the menu isn't open, run through opening procedure.
         */
        if(!this.menuOpen){
            this.menuOpen = true;
            menu.classList.add("open");
            menuColumns.classList.add("open");
            this._openColumns();
        }else{
            this.menuOpen = false;
            this._closeColumns();
        }

        this.vue["menuOpen"] = this.menuOpen;

    }

    /**
     * Opens up the column elements that form the menu.
     * @private
     */
    _openColumns(){
        // loop through and animate all column elements
        let columns = chibi.getNodes(".menu-column");

        for(let i = 0; i < columns.length; ++i){
            let el = columns[i];
            el.style.width = this.columnWidth + "px";
            el.style.opacity = 1;
            el.style.height = this.vue["resolution"][1] + "px";

            let links = document.querySelectorAll('.links');

            if(links){
               links[0]["style"].opacity = 1;
            }


        }

        // indicate to the rest of the site that the menu is open
        this.vue.$emit(events.MENU_OPEN);

        // set menu open
        // TODO this might not actually be needed if the menu sits on top of everything.
        this.vue["menuOpen"] = true;
    }

    /**
     * Closes the columns that form the menu.
     * @private
     */
    _closeColumns(){
        // loop through and animate all column elements
        let columns = chibi.getNodes(".menu-column");

        for(let i = 0; i < columns.length; ++i){
            let el = columns[i];
            el.style.width = 0 + "px";
            el.style.opacity = 0;
            el.style.height = this.vue["resolution"][1] + "px";

            let links = document.querySelectorAll('.links');

            if(links){
               setTimeout(()=>{
                   links[0]["style"].opacity = 0;
               })
            }
        }

        // indicate to the rest of the site that the menu is open
        this.vue.$emit(events.MENU_CLOSE);

        // set menu open
        // TODO this might not actually be needed if the menu sits on top of everything.
        this.vue["menuOpen"] = false;
    }

    /**
     * Resizes the menu when it's detected that the window is resizing.
     */
    resize(){
        let nav = document.querySelector("#menu");
        let resolution = this.vue["resolution"];

        // need a sec delay to allow resolution to get computed.
        setTimeout(() =>{
            nav["style"].width = resolution[0] + "px";
            nav["style"].height = resolution[1] + "px";

            this.resizeColumns();
        },200)

    }

    /**
     * Resize the columns that make up the navigation menu
     */
    resizeColumns(){

        let nav = document.querySelector("#menu");

        // calculate the how each column size should be. We want at least 4.
        let columnWidth = this.vue["resolution"][0] / this.numColumns;

        this.columnWidth = columnWidth;

        if(nav.querySelector("#nav-columns") === null){

            let menuColumns = document.createElement("div");
            menuColumns.id = "nav-columns";

            nav.appendChild(menuColumns);


            for(let i = 0; i < this.numColumns; ++i){
                let div = document.createElement("div");
                let canvas = document.createElement('canvas');

                div.className = `menu-column column-${i}`;
                div.style.transitionDelay = Math.random() + 1 + "s";
                div.style.left = `${columnWidth * i}px`;
                //div.style.width = columnWidth + "px";
                //div.setAttribute("data-column-size",columnWidth.toString());
                div.style.height = this.vue["resolution"][1] + "px";

                div.appendChild(canvas);

                menuColumns.append(div);
            }

            // append links to the first column
            let links = document.querySelector(".links");
            menuColumns.children[0].appendChild(links);

            this._adjustMenuCanvas();
        }else{

            let navColumnBox = chibi.getNodes("#nav-columns");
            let columns = navColumnBox.children;

            for(let i = 0; i < this.numColumns; ++i){
                let div = columns[i];
                //div.style.width = columnWidth + "px";

                if(navColumnBox.className.search("open") !== -1){
                    div.style.width = columnWidth + "px";
                }

                div.style.left = `${columnWidth * i}px`;
                div.setAttribute("data-column-size",columnWidth.toString());
                div.style.height = this.vue["resolution"][1] + "px";
            }

            this._adjustMenuCanvas();
        }

        this._generateMenuGradients();
    }

    /**
     * Runs when the page is loaded.
     */
    mounted(){

        // build out background
        this.resize();

        // setup menu link
        //let button = document.querySelector("#menu-button");
        //button["onclick"] = this.onClick.bind(this);

        this._generateMenuColors();

        // logo letter will trigger menu open, when it does so it will emit MENU_START_OPEN event.
        this.vue.$on(events.MENU_START_OPEN,this.onClick.bind(this));

        //! Make sure to resize when the window resizes.
        this.vue.$on(events.RESIZE,this.resize.bind(this));

    }

    /**
     * Adjusts the canvas elements within each menu column
     * @private
     */
    _adjustMenuCanvas(){
        let navColumnBox = chibi.getNodes("#nav-columns");
        let columns = navColumnBox.children;

        for(let i = 0; i < this.numColumns; ++i){
            let div = columns[i];
            let canvas = div.querySelector("canvas");
            canvas.width = this.columnWidth + 2;
            canvas.height = this.vue["resolution"][1];
        }


    }

    /**
     * Generates gradients used to create background for menu.
     * @private
     */
    _generateMenuGradients(){

        if(this.menuGradientColors.length === 0){
            this._generateMenuColors();
        }


        // calculate x/y values for each column and where to render each chunk of the gradient
        let positions = [];
        for(let i = 0; i < 4; ++i){

            positions.push({
                x:this.columnWidth * i,
                y:0,
                width:this.vue["resolution"][0],
                height:this.vue["resolution"][1]
            });
        }


        let width = this.vue["resolution"][0];
        let height = this.vue["resolution"][1];

        // finally render the gradient.
        let navColumnBox = chibi.getNodes("#nav-columns");
        let columns = navColumnBox.children;


        // sample random colors to add to stops
        let col1 = randInt(0,this.menuGradientColors.length - 1);
        let col2 = randInt(0,this.menuGradientColors.length - 1);
        let col3 = randInt(0,this.menuGradientColors.length - 1);



        for(let i = 0; i < this.numColumns; ++i){
            let div = columns[i];
            let canvas = div.querySelector("canvas");
            let ctx = canvas.getContext('2d');


            //let grad = ctx.createRadialGradient(100,0,100,0,0,300);
            let grad =  ctx.createLinearGradient(height,width,height,0.0);
            let position = positions[i];

            grad.addColorStop(0.083,this.menuGradientColors[col1].rgbString);
            grad.addColorStop(1,this.menuGradientColors[col2].rgbString);
            //grad.addColorStop(2,this.menuGradientColors[col3].rgbString);

            ctx.fillStyle = grad;
            ctx.fillRect(position.x * -1,position.y,position.width,position.height);
            ctx.save();

        }

        function randInt(min,max){
            return Math.floor(min + Math.random() * (max - min + 1));
        }
    }

    /**
     * Generates the colors used in the menu gradient
     * @private
     */
    _generateMenuColors(){
        let colors = [];
        for(let i = 0; i < 20; ++i){
            let col = randomColor({format:"rgb"});
            col = col.match(/\d+/g);

            colors.push({
                rgbString:`rgb(${col[0]},${col[1]},${col[2]}`,
                rgbValus:[
                    parseInt(col[0]),
                    parseInt(col[1]),
                    parseInt(col[2])
                ]
            });
        }

        this.menuGradientColors = colors;
    }
}