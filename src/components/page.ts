import {Component} from "../interfaces";
import Vue from 'vue'
import {convertTemplate} from "./component-helpers";
import events from "../events";

/**
 * The main page that the user will see.
 */
export default class implements Component {

    vue:Vue;

    /**
     * Builds the html tree.
     */
    tree(){
        return [
            [
                "div#load-panel"
            ],
            [
                "section#home-content-wrap",

                // div to hold current project title.
                ["section#project-title", ["div.wrap"]]
            ]
        ]
    }

    onClick(e){
        this.vue.$emit(events.MENU_START_OPEN);
    }

    /**
     * Renders the component
     * @param h
     * @param vue
     */
    render(h,vue){

        this.vue = vue;

        this.vue.$on(events.MOUNTED,this.mounted.bind(this));

        return convertTemplate(h,this.tree(),{
            domProps:{
                id:"home-content"
            }
        });

    }

    /**
     * Runs when the page is loaded.
     */
    mounted(){

        // bind listeners
        let title = document.querySelector("#title");
        title.addEventListener("click",this.onClick.bind(this));

    }
}