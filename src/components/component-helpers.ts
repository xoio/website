import {serialize} from "@thi.ng/hiccup";

/**
 * Converts Hiccup template to something Vue compatible
 * @param h {Function} Vue's createElement function commonly referenced as "h"
 * @param hiccup {Array} an hiccup style array structure containing your template info.
 * @param props {Object} any additional properties to pass onto Vue's render function. See https://vuejs.org/v2/guide/render-function.html for more detail
 */
export function convertTemplate(h,hiccup,props={}){
    let domProps = {
        innerHTML:serialize(hiccup)
    };

    if(props.hasOwnProperty("domProps")){
        props["domProps"]["innerHTML"] = serialize(hiccup);
    }else {
        props["domProps"] = domProps;
    }


    return h('div',props);
}

/**
 * Splits text into separate chunks
 * @param text {string} text you want to split
 */
export function charSplit(text:string){
    let title = text.split("")
    return title.map(char => {
        return `<span class="char">${char}</span>`
    });
}

/**
 * Same thing as regular charSplit but returns array to be hicccup compatible
 * @param text {string} test to split.
 */
export function charSplitHiccup(text:string){
    let title = text.split("")
    return title.map(char => {
        return ["span",{class:"char"},char]
    });
}

/**
 * For content - converts ID to a proper title. Capitalizes first letter of each section
 * @param id {string} id to convert
 */
export function idToTitle(id:string){
    let t = id.split("-");

    let splits = t.map(sec => {
        let cpa = sec.charAt(0).toUpperCase();
        let spl = sec.split("");
        spl[0] = cpa;
        return spl.join("");
    });

    return splits.join(" ");
}

/**
 * When splitting a title, this will randomly place the character out of view
 */
export function setHiddenTitle(hiccup){

    return hiccup.map(chunk => {
        let final = [
            chunk[0],
            {
                class:chunk[1].class + " " + (Math.random() > 0.5 ? "hide-up" : "hide-down")
            },
            chunk[2]
        ];

        return final;
    })
}



export function lookupProject(projects,projectid){
    let project = null;
    for(let i in projects){
        if(i === projectid){
            project = projects[i]
        }
    }

    return project;
}