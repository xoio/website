/**
 * Constant values used to keep track of events.
 * Done here to reduce chance of error due to mis-spelling, etc.
 */
export default {

    // triggered when keydown event is triggered.
    KEYDOWN:"KEYDOWN",

    /// triggered when the page is resized
    RESIZE:"RESIZE",

    // triggered when the resize event is finished (fires approximately 200 ms after the end of resizing)
    RESIZE_COMPLETE:"RESIZE_COMPLETE",

    // triggered when there is a scroll event
    SCROLL:"SCROLL",

    // triggered when Vue runs it's "mounted" event.
    MOUNTED:"MOUNTED",

    // tied to vue even "updated" - signals when the dom is refreshed
    DOM_UPDATE:"DOM_UPDATE",

    // triggered whenever the mouse moves
    MOUSE_MOVE:"MOUSE_MOVE",

    // triggered whenever the mouse button is held down.
    MOUSE_DOWN:"MOUSE_DOWN",

    // triggered whenever the mouse button is held up.
    MOUSE_UP:"MOUSE_UP",

    // triggered when the mouse wheel is rolled.
    MOUSE_WHEEL:"MOUSE_WHEEL",

    // triggered when we've stopped scrolling
    MOUSE_WHEEL_COMPLETE:"MOUSE_WHEEL_COMPLETE",

    // triggered when all the thumbnails have loaded.
    THUMBNAILS_LOADED:"THUMBNAILS_LOADED",

    // triggered when the menu is open
    MENU_OPEN:"MENU_OPEN",

    // triggered when the menu is closed.
    MENU_CLOSE:"MENU_CLOSE",

    // indicates that the menu should start to open
    MENU_START_OPEN:"MENU_START_OPEN"
}