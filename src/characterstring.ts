

export default class CharacterString {

    // raw content of the character string
    content:string;

    // DOM elements that make up the character string
    elements:Array<any> = [];

    // any options for how to present the character string
    options:any;

    constructor(text:string,{
        characterDimensions=10,
        randomCharacterDelay=true,
        characterTransitionTime=0.5,
        easeType="ease",
        characterClassName="character-char"
    }={}){

        this.content = text;
        this.options = {
            characterDimensions:characterDimensions,
            randomCharacterDelay:randomCharacterDelay,
            characterTransitionTime:characterTransitionTime,
            easeType:easeType,
            characterClassName:characterClassName
        };

        this.elements = text.split("").map(char => {
            let val = document.createElement("div");
            val.style.display = "inline";
            val.style.paddingLeft = "1px"
            val.style.paddingRight = "1px"
            val.style.width = this.options.characterDimensions + "px";
            val.style.height = this.options.characterDimensions + "px";
            val.style.overflow = "hidden";
            val.style.position = "relative";

            // debug
            val.style.background = "Red";
            val.style.marginLeft = "2px";


            let inner = document.createElement("span");
            inner.style.display = "inline-block"
            inner.style.top = 0 + "px"
            inner.innerHTML = char;
            inner.style.position = "relative";

            let delay = 0.0;
            if(randomCharacterDelay){
                delay = Math.random();
            }

            inner.setAttribute("data-delay",delay.toString());

            inner.style.transition = `all ${characterTransitionTime * delay}s ease`

            // assign a class name so we can manipulate w/ css
            inner.className = characterClassName;

            // debug
            inner.style.color = "#ffffff"

            val.appendChild(inner);
            return val;
        })
    }

    appendTo(el:HTMLElement){
        this.elements.forEach(char => {
            el.appendChild(char);
        });
    }


    hideCharacters(onComplete:Function){

        let count = this.elements.length;
        let finished = 0;

        this.elements.forEach(char => {
            let el = char.children[0];
            el.style.top = `${this.options.characterDimensions + 5}px`;
            let transition = () => {

                finished += 1;
                if(finished === count){
                    onComplete();
                }
            };

            el.addEventListener("transitionend",transition)
        })

    }

    showCharacters(onComplete:Function){
        let count = this.elements.length;
        let finished = 0;

        this.elements.forEach(char => {
            let el = char.children[0];
            el.style.top = `0px`;
            let transition = () => {

                finished += 1;
                if(finished === count){

                    onComplete();
                }
            };

            el.addEventListener("transitionend",transition)
        })
    }
}