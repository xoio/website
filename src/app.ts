import Loader from "./gl/AssetLoaderr";

const s  = require('./styles/main.scss');
import ObjLoader from './gl/jirachi/loaders/ObjLoader'
import * as data from './data.json'

let loader = new ObjLoader("./assets/petal.txt")
const useFonts = false;

let content = data["default"];

// enable some debugging strings when building in development.
if(process.env.NODE_ENV === "development"){
    // debug flag
    window["debug"] = true;
}

if(window.Typekit && useFonts){
    Typekit.load({
        loading(){

            console.log("loading fonts")
        },
        inactive(){

            console.log("Fonts requested are inactive")
        },
        active(){
            /**
             * Bootstrap site anyways and start loading anything we need
             */
            import("./controller").then(Controller => {
                loadSite(Controller);
            });
        }
    });

}else{
    /**
     * Bootstrap site anyways and start loading anything we need
     */
    import("./controller").then(Controller => {
        loadSite(Controller);
    });
}

/**
 * Takes care of finishing up loading the site as well as preparing any global content
 * in an easier to digest format.
 * @param Controller {Controller} a Controller object instance.
 * @param globalAssets {Array} an array of global assets to load.
 */

function loadSite(Controller,globalAssets=[

]){

    // load assets
    let assets = []
    for(let a in content){
        let project = content[a];
        let _assets = project.assets;
        let thumb = new Image();
        thumb.src = project.thumb;

        _assets.forEach(asset =>{
            let _img = new Image();
            _img.src = asset;
            assets.push({
                id:a,
                img:_img
            })
        })

        assets.push({
            id:a,
            img:thumb
        })


    }

    Loader.loadAssets(assets).then(assets =>{

        // wipe old assets and replace with actual images
        for(let a in content){
            content[a].assets = [];
        }

        assets.forEach(asset =>{
            for(let a in content){
                if(asset["id"] === a){
                    content[a].assets.push(asset["img"]);
                }
            }
        })

        // load global assets if necessary, otherwise initialize controller.
        if(globalAssets.length > 0){
            Loader.loadAssets(globalAssets).then(gAssets => {
                new Controller.default().start(content,gAssets);
            })
        }else{
            new Controller.default().start(content,globalAssets);
        }


    })

}