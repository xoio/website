precision highp float;
uniform sampler2D uTex0;
in  vec2 vUv;
out vec4 glFragColor;
uniform float sceneAlpha;
void main(){

    vec4 data = texture(uTex0,vUv);
    glFragColor = data;
}