precision highp float;

in vec4 vColor;
out vec4 glFragColor;

uniform vec2 resolution;
uniform sampler2D uTex0;

void main() {
    vec2 uv = gl_FragCoord.xy / resolution.xy;

    glFragColor = texture(uTex0,uv);
    //glFragColor = vec4(1.);
}
