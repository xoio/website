precision highp float;


uniform sampler2D uCurrentImage;
uniform sampler2D uNextImage;
uniform sampler2D mixTexture;
uniform float threshold;
uniform float mixRatio;
uniform bool startTransition;

out vec4 glFragColor;
in vec2 vUv;



void main() {
    // main plane uv
    vec2 uv = vUv;

    // sample textures.
    vec4 data = texture(uCurrentImage,vUv);
    vec4 data2 = texture(uNextImage,vUv);

    vec4 transition= texture( mixTexture, vUv );
    float r = mixRatio * (1.0 + threshold * 2.0) - threshold;
    float mixf = clamp((transition.r - r) * (1.0 / threshold),0.0,1.0);
    glFragColor = mix( data, data2, mixf );

}
