uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

layout(location = 0) in vec3  position;
uniform float time;
void main() {



    gl_PointSize = 5.0;
    gl_Position = projectionMatrix * viewMatrix * vec4(position,1.);// * vec4(0.);
}
