
uniform float time;
uniform bool mouseDown;
uniform vec3 uMousePos;
uniform float uMouseForce;

layout(location = 0) in vec4 iPosition;
layout(location = 1) in vec4 iHomePosition;
layout(location = 2) in vec4 iPPosition;
layout(location = 3) in vec4 iDamping;

out vec4 position;
out vec4 homePosition;
out vec4 pposition;
out vec4 damping;

const vec3 OFFSET = vec3(2399.24849823098, 3299.9028381, 389.09338327);
const float SPEED = 2.0;
const float dt2 = 1.0 / (60.0 * 60.0);

void main() {

    float t = time + 100000.0;

    vec4 pos = iPosition;
    vec4 home = iHomePosition;



    float x = snoise(vec3(iPosition.xy * 0.005, 9280.03992092039 + t ) + OFFSET);
    float y = snoise(vec3(position.xy * 0.005, 3870.73392092039+ t ) + OFFSET);

    float radius = length(iPosition.xyz) * 0.005;
    float rad = 0.0002 * radius;


    pos.xy += vec2(
    snoise(vec3(pos.xy * 0.005, 9280.03992092039 + t ) + OFFSET)
    , snoise(vec3(pos.xy * 0.005, 3870.73392092039 - t ) + OFFSET)
    ) * SPEED;

    pos.xy = vec2(
    pos.x * cos(rad) - iPosition.y * sin(rad)
    , iPosition.y * cos(rad) + iPosition.x * sin(rad)
    );



    ///////
    position = pos;
    homePosition = iHomePosition;
    pposition = iPosition;
    damping = iDamping;
}


/*

    One posibility but then we have to contend with drift if we don't reset or spin the particles.

  float x = snoise(vec3(iPosition.xy * 0.005, 9280.03992092039 + t ) + OFFSET);
    float y = snoise(vec3(position.xy * 0.005, 3870.73392092039+ t ) + OFFSET);

    float radius = length(iPosition.xyz) * 0.005;
    float rad = 0.0002 * radius;

    vec4 pos = iPosition;
    vec4 home = iHomePosition;


    pos.xy += vec2(
    snoise(vec3(pos.xy * 0.005, 9280.03992092039 + t ) + OFFSET)
    , snoise(vec3(pos.xy * 0.005, 3870.73392092039 - t ) + OFFSET)
    ) * SPEED;

    pos.xy = vec2(
    pos.x * cos(rad) - iPosition.y * sin(rad)
    , iPosition.y * cos(rad) + iPosition.x * sin(rad)
    );

*/