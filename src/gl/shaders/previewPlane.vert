
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

in vec3 position;
in vec2 uv;

out vec2 vUv;
void main() {
    vUv = uv;
    gl_Position = projectionMatrix * viewMatrix * vec4(position,1.);
}
