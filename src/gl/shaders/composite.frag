precision highp float;

uniform float time;
uniform vec2 resolution;
out vec4 glFragColor;

void main(){
    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = gl_FragCoord.xy/resolution.xy;

    // Time varying pixel color
    vec3 col = 0.5 + 0.5*cos(time+uv.xyx+vec3(0,2,4));

    glFragColor = vec4(col,1.);
}