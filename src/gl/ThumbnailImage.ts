import {Vue} from "vue/types/vue";
import Mesh from './jirachi/framework/mesh'
import Plane from './jirachi/geometry/plane'
import createShader from './jirachi/core/shader'
import createTexture from './jirachi/core/texture'
import CharacterString from '../characterstring'
import * as dat from 'dat.gui'

import {PerspectiveCamera} from './jirachi/framework/camera'
import vert from './shaders/previewPlane.vert'
import frag from './shaders/previewPlane.frag'
import {ShaderFormat, TextureFormat} from "./jirachi/core/formats";
import events from "../events";
import mat4 from "./jirachi/math/mat4";

let gui;
if(window["debug"]){
    gui = new dat.GUI();
}

/**
 * Responsible for showing all thumbnails
 * Ideas for look
 * https://www.shadertoy.com/view/wtdGDs
 *https://www.shadertoy.com/view/MsdGWn
 */
export default class ThumbnailImage{

    // holds current mouse position.
    cameraPosition:Array<number> = [0,0,0];

    // custom view matrix to use.
    viewMatrix:Array<any> = mat4.create();

    // reference to the WebGL context.
    gl:any;

    // reference to the main vue instance
    vue:Vue;

    // current image we're looking at
    currentIndex:number = 0;

    // array of images to show
    images:Array<any> = [];

    // mesh to show stuff on
    display:Mesh;

    // scale to help set image based on the current full width of the canvas.
    imageScale:number;

    // camera used to view things
    camera:PerspectiveCamera;

    // shader used to show the image on screen.
    viewShader:any;

    // settings to use for the animation transition.
    transitionSettings:Object = {

        transitionImage:new Image(),
        transitionLoaded:false,
        mixRatio:0.0,
        threshold:0.1,
        transition:1,
        transitionSpeed:0.5,
        textureThreshold:0.3

    };

    // holds a reference to the current image we want to show.
    currentImage:any = undefined;

    // holds a reference to the image we want to transition to when the scroll event is triggered on desktop.
    nextImage:any = undefined;

    // holds a reference to the image we want to scroll to if we scoll backwards.
    previousImage:any  = undefined;

    // a flag to help prevent any additional animations from happening when a user wanted to go to the next/previous image
    startImageTransition:boolean = false;

    // the direction the user goes in when transitioning to a new image from teh current image
    imageDirection:any = null;

    // id of selector to attach titles to
    titleElementSelector:string = "#project-title";

    // element object for title container
    titleElement:HTMLElement;


    constructor(gl,vue,images,scale=0.7){


        if(window["debug"]){
            gui.add(this.transitionSettings,"transition",0,1.0,0.01);
        }

        // TODO load this in AssetLoader, not sure why it's not working at the moment.
        this.transitionSettings["transitionImage"].src = "./assets/transition.png";
        this.transitionSettings["transitionImage"].onload = () => {
            this.transitionSettings["transitionImage"] = createTexture(this.gl,new TextureFormat(this.gl,{
                data:this.transitionSettings["transitionImage"]
            }));

            this.transitionSettings["transitionLoaded"] = true;
        };

        this.gl = gl;
        this.vue = vue;
        this.imageScale = scale;
        this.images = images;

        this.titleElement = document.querySelector(this.titleElementSelector);



        // make sure to store references to image original width / height
        // also process and create character strings
        this.images.forEach(img => {
            img.width = img.tex.format.width;
            img.height = img.tex.format.height;
            img.copy = new CharacterString(img.title,{
                characterDimensions:40
            });
        })

        this._setup();
    }


    /**
     * Reinitializes the display mesh due to any change in width or height of the viewport.
     * @param width {number} the new scaled width of the display area
     * @param height {number} the new scaled height of the display area.
     */
    rebuild(width,height){
        this.display = new Mesh(this.viewShader,new Plane(width,height));
    }

    /**
     * Sets up the plane
     * @private
     */
    _setup() {

        // setup image objects to get properly injected into the shader
        this._updateImageObjects();


        // set the current title in the title box.

        let current = this.images[this.currentIndex];
        this._attachTitle();


        let img = this.currentImage;

        let imageScaledWidth = img.width * this.imageScale;
        let imageScaledHeight = img.height * this.imageScale;

        let aspect = this.gl.canvas.width / this.gl.canvas.height;

        imageScaledWidth *= aspect;
        imageScaledHeight *= aspect;

        this.viewShader = createShader(this.gl, new ShaderFormat(vert, frag));

        this.display = new Mesh(this.viewShader,new Plane(imageScaledWidth,imageScaledHeight));

        // setup camera
        this.camera = new PerspectiveCamera({
            near:0.1,
            far:10000.0,
            fov:70.0,
            aspect: this.gl.canvas.width / this.gl.canvas.height,
        });
        this.camera.setZoom(2000);

        this.vue.$on(events.RESIZE,()=>{
            // resize camera based on the current viewport size.
            this.camera.resize(this.gl.canvas.width / this.gl.canvas.height);


            let img = this.images[this.currentIndex]
            let imageScaledWidth = img.width * this.imageScale;
            let imageScaledHeight = img.height * this.imageScale;

            let aspect = this.gl.canvas.width / this.gl.canvas.height;

            imageScaledWidth *= aspect;
            imageScaledHeight *= aspect;

            this.rebuild(imageScaledWidth,imageScaledHeight);

        });

       if(!this.vue["isMobile"]){
           // add listener for keydown events.
           this.vue.$on(events.KEYDOWN,e => {

               // TODO not sure if ArrowUp/ArrowDown is properly supported everywhere outside of Firefox/Chrome - need to research later.
               switch(e.key){
                   case "ArrowUp":
                       this.imageDirection = "up";
                       break;

                   case "ArrowDown":
                       this.imageDirection = "down";
                       break;
               }

               this._attachTitle(()=>{
                   // TODO probably could get away with TweenLite - wait to see what else we might need to use this on.
                   TweenMax.to(this.transitionSettings,1,{
                       transition:0,
                       onComplete:this._transitionComplete.bind(this)
                   })
               })



           });



           // add listener for mousewheel events.
           this.vue.$on(events.MOUSE_WHEEL,e => {
               if(e.triggered && e.direction){


                   switch(e.direction){
                       case "up":
                           this.imageDirection = "up";
                           break;

                       case "down":
                           this.imageDirection = "down";
                           break;
                   }


                   this._attachTitle(() => {
                       // TODO probably could get away with TweenLite - wait to see what else we might need to use this on.
                       TweenMax.to(this.transitionSettings,1,{
                           transition:0,
                           onComplete:this._transitionComplete.bind(this)
                       })
                   })

               }
           });


           // setup mouse events
           this.vue.$on(events.MOUSE_MOVE, (x,y) => {

               this.cameraPosition[0] = x;
               this.cameraPosition[1] = y;

           })
       }
    }

    updateCamera(){
        let camera = this.camera;
        let x = this.cameraPosition[0];
        let y = this.cameraPosition[1];

        let _x = camera.getX() + (x - camera.getX()) * 0.05;
        let _y = camera.getY() + ( -y - camera.getY()) * 0.05;

        camera.translate([_x,_y,0]);

    }


    /**
     * Appends title to the DOM
     * @param cb {function} function to call once title has animated in. Default is to do nothing.
     * @private
     */
    _attachTitle(cb:Function=()=>{}){

        // get the title of the next thing we're about to show.
        let idx = 0;
        switch (this.imageDirection) {
            case "up":
                idx = this._getPreviousImage()
                break;

            case "down":
                idx = this._getNextImage();
                break;
        }

        let current = this.images[idx];

        this.titleElement.children[0].innerHTML = "";

        current.copy.appendTo(this.titleElement.children[0]);


        cb();
    }

    /**
     * callback for TweenMax to reset properties in preparation for next transition.
     * @param e
     * @private
     */
    private _transitionComplete(e){
        this.startImageTransition = false;

        this._updateImages();
        this.imageDirection = "";
        this.transitionSettings["transition"] = 1;
    }

    /**
     * Helper to quickly grab image objects for each possible stage.
     * @private
     */
    _updateImageObjects(){

        this.currentImage = this.images[this.currentIndex];
        this.nextImage = this.images[this._getNextImage()];
        this.previousImage = this.images[this._getPreviousImage()];
    }


    /**
     * Updates the indices we need to use after animating to another image.
     * Call after animation completes.
     * @private
     */
    _updateImages(){

        if(this.imageDirection === "up"){
            this.currentIndex = this._getPreviousImage();
        }else if(this.imageDirection === "down"){
            this.currentIndex = this._getNextImage();
        }



        this._updateImageObjects();
    }


    _getNextImage(){
        if(this.currentIndex < this.images.length - 1){
            return this.currentIndex + 1;
        }else{
            return 0;
        }
    }
    _getPreviousImage(){

        if(this.currentIndex > 0){
            return this.currentIndex - 1;
        }else{
            return this.images.length - 1;
        }

    }

    draw(){
        this.currentImage.tex.bind(0);

        this.updateCamera();
        // bind transition texture
        if(this.transitionSettings["transitionLoaded"]){
            this.transitionSettings["transitionImage"].bind(2);
        }

        // if we've started transitioning to the next image, bind the correct texture as necessary.
        // by default we bind the following texture.
        if( this.imageDirection !== ""){
            if(this.imageDirection === "up"){
                this.previousImage.tex.bind(1);
            }else {
                this.nextImage.tex.bind(1);
            }
        }

        this.display.draw(this.camera, shader => {
            shader.int("uCurrentImage",0);
            shader.int("uNextImage",1);
            shader.int("mixTexture",2);

            shader.int("startTransition",this.startImageTransition);


            // apply all transition settings.
            shader.float("mixRatio",this.transitionSettings["transition"]);
            shader.float("threshold",this.transitionSettings["threshold"]);
        });


    }

    /*
          this.currentImage.tex.bind(0);

        // if we've started transitioning to the next image, bind the correct texture as necessary.
        // by default we bind the following texture.
        if(this.startImageTransition && this.imageDirection !== ""){
            if(this.imageDirection === "up"){
                this.previousImage.tex.bind(1);
            }else{
                this.nextImage.tex.bind(1);
            }
        }else{
            this.nextImage.tex.bind(1);
        }

        // bind transition texture
        if(this.transitionSettings["transitionLoaded"]){
            this.transitionSettings["transitionImage"].bind(2);
        }

        this.display.draw(this.camera, shader => {
            shader.int("uCurrentImage",0);
            shader.int("uNextImage",1);
            shader.int("mixTexture",2);

            shader.int("startTransition",this.startImageTransition);


            // apply all transition settings.
            shader.float("mixRatio",this.transitionSettings["transition"]);
            shader.float("threshold",this.transitionSettings["threshold"]);
        });

     */


}

