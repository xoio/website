import {Vue} from "vue/types/vue";
import createFBO from './jirachi/core/fbo'
import Mesh from './jirachi/framework/mesh'
import createShader from './jirachi/core/shader'

import debugVert from './shaders/debug/previewPlane.vert'
import debugFrag from './shaders/debug/previewPlane.frag'

import {ShaderFormat, TextureFormat} from "./jirachi/core/formats";

import ParticleImage from './particleImage'
import events from "../events";
import Quad from "./jirachi/geometry/quad";
import ThumbnailImage from './ThumbnailImage'


/**
 * This manages a thumbnail preview and generates the final visuals.
 */
export default class ThumbnailController {
    gl:any;
    projects:any;
    thumbnails:Array<any> = [];
    vue:Vue;

    // particles that are supposed to dance around image in interesting ways.
    particleImage:ParticleImage;

    // fbo to render out preview image so it can scale.
    previewFBO:any;
    renderMesh:any;


    // object to manage viewing image thumbnails.
    thumbnailImageViewer:ThumbnailImage;
    constructor(gl,vue,thumbnails){

        this.gl = gl;
        this.vue = vue;
        this.thumbnails = thumbnails;

        this._setup();

    }

    draw(camera){


        this.previewFBO.bind();
        this.gl.clearScreen();
        this.thumbnailImageViewer.draw();
        this.previewFBO.unbind();

        this.particleImage.draw(this.previewFBO.getColorTexture());
    }

    /**
     * calculate the scaled dimensions to show the preview plane.
     * @private
     */
    _getScaledDimensions(){
        // generate the preview plane.
        let width = this.gl.canvas.width;
        let height = this.gl.canvas.height;

        // scale
        let pct = 0.8;
        let scaledWidth = width * pct;
        let scaledHeight = height * pct;

        return {
            width:scaledWidth,
            height:scaledHeight
        }
    }

    /**
     * Rebuild the preview plane used to show thumbnais.
     * @private
     */
    _buildThumbnailPreviewPlane(){
        let scaledDimensions = this._getScaledDimensions();
        //this.thumbnailImageViewer.rebuild(scaledDimensions.width,scaledDimensions.height);
    }

    /**
     * Sets things up
     * @private
     */
    _setup(){

        // generate the preview plane.
        let width = this.gl.canvas.width;
        let height = this.gl.canvas.height;
        let scaledDimensions = this._getScaledDimensions();
        this.particleImage = new ParticleImage(this.gl,this.vue,scaledDimensions.width,scaledDimensions.height);

        this.previewFBO = createFBO(this.gl,new TextureFormat(this.gl,{
            width:width,
            height:height
        }));

        this.previewFBO.resize(this.gl.canvas.width,this.gl.canvas.height);

        // ensure fbo resizes when the window changes.
        this.vue.$on(events.RESIZE,()=>{
            this.previewFBO.resize(this.gl.canvas.width,this.gl.canvas.height);
        });

        this.vue.$on(events.RESIZE_COMPLETE,() =>{

            this._buildThumbnailPreviewPlane();
        })


        let shader = createShader(this.gl,new ShaderFormat(debugVert,debugFrag));

        // build rendering mesh.
        this.renderMesh = new Mesh(shader, new Quad());

        // build thumbnail viewer.
        this.thumbnailImageViewer = new ThumbnailImage(this.gl,this.vue,this.thumbnails);
    }
}
