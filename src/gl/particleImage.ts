import createVao from './jirachi/core/vao'
import createVbo from './jirachi/core/vbo'
import createShader from './jirachi/core/shader'
import {PerspectiveCamera} from './jirachi/framework/camera'
import mat4 from './jirachi/math/mat4'
import {toRadians} from "./jirachi/math/core";
import Plane from './jirachi/geometry/plane'
import imageVert from './shaders/image.vert'
import imageFrag from './shaders/image.frag'
import noiseShader from './jirachi/shaders/noise/noise3D.glsl'
import updateShader from './shaders/imageUpdate.glsl'
import {ShaderFormat} from "./jirachi/core/formats";
import {Vue} from "vue/types/vue";
import events from "../events";
import {randFloat} from "./jirachi/math/core";


// sample https://github.com/WebGLSamples/WebGL2Samples/blob/master/samples/transform_feedback_interleaved.html#L105-L231
export default class ParticleImage {
    gl:any;


    vaos:Array<any> = [];
    vbos:Array<any> = [];

    // the length of each value in the vbo
    byteLength:number;

    // reference to rendering shader
    shader:any;

    // reference to update shader for transform feedback
    updateShader:any;

    // number of particles to render
    numVertices:number;

    // indicates which vbo/vao is in use
    flag:number = 0;

    // transform feedback object.
    tf:any;

    // reference to the last image being shown.
    lastImage:any;

    // when we should be animating vs showing an image
    shouldShowImage:boolean = false;

    vue:Vue;


    camera:PerspectiveCamera;

    mousePosition:Array<number> = [];
    mouseDown:boolean = false;
    mouseForce:number = 0.0;
    constructor(gl,vue,width,height){
        this.gl = gl;
        this.tf = gl.createTransformFeedback();

        this._buildShader();
        this._buildData(width,height);


        this.vue = vue;

        this.camera = new PerspectiveCamera({
            near:0.1,
            far:10000.0,
            aspect: this.gl.canvas.width / this.gl.canvas.height,
        });
        this.camera.setZoom(-700);
        this.camera.setZoom(-700);

        vue.$on(events.MOUSE_MOVE,(x,y)=>{
            this.mousePosition = [x,y, 0];

            let deltaY = this.mousePosition[1] - 10;
            let deltaX = this.mousePosition[0];

            let ang = Math.atan2(deltaY,deltaX) * 180 / 3.141



        })

        vue.$on(events.MOUSE_UP,() => {
            this.mouseDown = false;
            this.mouseForce = 0.0;
        })

        vue.$on(events.MOUSE_DOWN,() =>{
            this.mouseDown = true;
            this.mouseForce = 150.0;
        })
    }

    rebuild(width,height){
        this._buildData(width,height);
    }

    draw(currentImage=null){
        this._updatePositions();

        let gl = this.gl;

        this.shader.bind();

        if(currentImage !== null && currentImage instanceof WebGLTexture){
            currentImage["bind"](0);
        }

        this.shader.mat4("projectionMatrix",this.camera.projectionMatrix);
        this.shader.mat4("viewMatrix",this.camera.viewMatrix);
        this.shader.uniform("resolution",this.vue["resolution"]);

        this.vaos[this.flag].bind();
        gl.drawArrays(gl.POINTS,0,this.numVertices);

        this.vaos[this.flag].unbind();
    }

    /**
     * Run transform feedback.
     * @private
     */
    _updatePositions(){

        let gl = this.gl;
        let src = this.flag;
        let dst = (this.flag + 1) % 2;

        this.updateShader.bind();

        this.updateShader.float("time",performance.now() * 0.005);

        this.updateShader.int("shouldAnimate",this.shouldShowImage);
        this.updateShader.uniform("uMousePos",this.mousePosition);
        this.updateShader.int("mouseDown",this.mouseDown);
        this.updateShader.float("uMouseForce",this.mouseForce)

        this.vaos[src].bind();

        // discard fragment stage
        gl.enable(gl.RASTERIZER_DISCARD);

        // start transform feedback
        gl.bindTransformFeedback(gl.TRANSFORM_FEEDBACK, this.tf);

        // bind all the buffers to read data from transform feedback process
        gl.bindBufferBase(gl.TRANSFORM_FEEDBACK_BUFFER, 0,this.vbos[dst]);


        // start transform feedback
        gl.beginTransformFeedback(gl.POINTS);

        gl.drawArrays(gl.POINTS, 0, this.numVertices);
        gl.endTransformFeedback();

        this.vaos[src].unbind();
        gl.bindBufferBase(gl.TRANSFORM_FEEDBACK_BUFFER, 0,null);
        gl.bindTransformFeedback(gl.TRANSFORM_FEEDBACK, null);

        gl.disable(gl.RASTERIZER_DISCARD);
        this.flag =  (this.flag + 1) % 2;
    }
    /**
     * Update shader for the plane.
     * @private
     */
    _buildShader(){


        this.shader = createShader(this.gl,new ShaderFormat(imageVert,imageFrag));
        this.updateShader = createShader(this.gl,new ShaderFormat([noiseShader,updateShader])
            .setVaryings([
                "position",
                "homePosition",
                "pposition",
                "damping"
            ])
            .setFeedbackMode(this.gl.INTERLEAVED_ATTRIBS));
    }


    _applyData(data){
        let gl = this.gl;

        let arr = new Float32Array(data);

        this.byteLength = arr.BYTES_PER_ELEMENT;

        let vertexStep = this.byteLength * 16;


        for(let i = 0 ;i < 2; ++i){

            this.vaos[i] = createVao(this.gl);
            // bind vao and build buffer for attribute
            this.vaos[i].bind();

            this.vbos[i] = createVbo(this.gl,false,arr);

            gl.enableVertexAttribArray(0)
            gl.enableVertexAttribArray(1)
            gl.enableVertexAttribArray(2)
            gl.enableVertexAttribArray(3)


            this.vbos[i].bind();
            gl.vertexAttribPointer(0,4,gl.FLOAT,gl.FALSE,vertexStep,0);
            gl.vertexAttribPointer(1,4,gl.FLOAT,gl.FALSE,vertexStep,this.byteLength * 4);
            gl.vertexAttribPointer(2,4,gl.FLOAT,gl.FALSE,vertexStep,this.byteLength * 8);
            gl.vertexAttribPointer(3,4,gl.FLOAT,gl.FALSE,vertexStep,this.byteLength * 12);



            this.vbos[i].unbind();


            this.vaos[i].unbind();

        }
    }


    /**
     * Build interleaved data of all attributes we need to be aware of.
     * Data should look something like
     * [ 0.0,0.0,0.0, // position
     *   0.0,0.0,0.0, // color
     *   ]
     * @private
     */
    _buildData(width,height){


        let p = new Plane(width,height,4,4);


        let positions = [];

        let res = 1;
        let nx = width / res;
        let ny = height / res;
        let sx = width;
        let sy = height;

        for (let iy = 0; iy <= ny; iy++) {
            for (let ix = 0; ix <= nx; ix++) {
                let u = ix / nx
                let v = iy / ny

                let x = -sx / 2 + u * sx // starts on the left
                let y = sy / 2 - v * sy // starts at the top

                positions.push(
                    // position
                    x,y,1,1,

                    // original position (for when showing an image)
                    x-1,y,1,1,

                    // home position + random dampening
                    x,y,1,1,

                    //  dampening
                    randFloat(0.965,0.985),0,0,0

                );
            }
        }

        this.numVertices = positions.length / 16;

        this._applyData(positions);




    }
}
