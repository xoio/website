import TransformFeedback from './jirachi/framework/TransformFeedback'
import createShader from './jirachi/core/shader'
import {flattenArray, randVec3, randVec4} from "./jirachi/math/core";
import {ShaderFormat} from "./jirachi/core/formats";

// import shaders
import vert from './shaders/petalSim.glsl'
import rVert from './shaders/petalRender.vert'
import rFrag from './shaders/petalRender.frag'
import rotate from './jirachi/shaders/utils.glsl'
import noise from './jirachi/shaders/noise/curl.glsl'
import Mesh from "./jirachi/framework/mesh";

export default class ParticleSystem{
    gl:any;
    numParticles:number;
    tf:TransformFeedback;
    renderShader:any;
    petal:Mesh;
    resolution:[number,number];
    
    constructor(gl,geo,resolution:[number,number],numParticles=100) {
        this.gl = gl;
        this.numParticles = numParticles;

        this.tf = new TransformFeedback(gl,{
            numItems:numParticles
        });

        let simShader = [
            noise,
            vert
        ].join("\n");

        this.resolution = resolution;

        // set transform feedback shader.
        this.tf.setShader(simShader,[
            "oPos",
            "oVel"
        ],["time"]);


        this.renderShader = createShader(gl,new ShaderFormat()
            .vertexSource([
                rotate,
                rVert
            ].join("\n"))
            .fragmentSource(rFrag));

        this._loadGeometry(geo)

    }

    setResolution(res:[number,number]){
        this.resolution = res;
    }

    resize(w,h){
        this.resolution = [w,h];
    }

    draw(camera){
        let perf = performance.now() * 0.005;
        // TODO enable alpha blending

        // update transform feedback
        this.tf.update();
        this.tf.shader.float("time",performance.now() * 0.0005);



        this.petal.draw(camera);
        this.petal.shader.float("time",perf);
        this.petal.uniform("projectionMatrix",camera.projectionMatrix)
            .uniform("resolution",this.resolution);
    }

    _build(){

        let positions = [];
        let velocity = [];
        let originData = [];

        for(let i = 0; i < this.numParticles; ++i){

            // setup particle position
            let position = randVec4();

            // setup particle velocity
            let vel = [0,0,0]

            positions.push(...position);
            velocity.push(...vel);
            originData.push(position[0],position[1],position[2]);
        }

        // add attributes to keep track of.
        // note - position w will hold particle life.
        this.tf.addAttribute("position",positions,{
            size:4
        });
        this.tf.addAttribute("velocity",velocity);

        this.tf.addAttribute("origin",originData);

    }

    /**
     * Load petal geometry
     * @param assets
     * @private
     */
    _loadGeometry(petal){


        this._build();
        let petalGeo = petal;
        // flatten arrays
        let positions = flattenArray(petalGeo.positions);
        let uvs = flattenArray(petalGeo.coords,2);

        // setup random scale
        let scale = [];
        for(let i = 0; i < this.numParticles;++i){
            scale.push(Math.random() + 1.0);
        }


        this.petal = new Mesh(this.renderShader)
        this.petal.addAttribute("position",positions);
        this.petal.addInstancedAttributeBuffer("iPosition",this.tf.getAttributeData("position"),{
            size:4,
            numItems:100
        });
        this.petal.addInstancedAttribute("iScale",scale,{
            size:1
        });


        this.petal.numItems = this.numParticles;

    }
}