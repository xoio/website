import createRenderer from './jirachi/core/gl'
import createShader from './jirachi/core/shader'
import createTexture from './jirachi/core/texture'
import {PerspectiveCamera} from './jirachi/framework/camera'

import Mesh from './jirachi/framework/mesh'
import events from "../events";
import {TextureFormat,ShaderFormat} from "./jirachi/core/formats";

import ThumbnailController from './ThumbnailController'
import Quad from "./jirachi/geometry/quad";

import compositeVert from './shaders/composite.vert'
import compositeFrag from './shaders/composite.frag'

/**
 * Manages all WebGL aspects
 */
class GLController {

    // main camera for GL elements
    camera:any;

    // reference to the main Vue instance running the show.
    vue:any;

    // a reference to the GL context.
    gl:any;

    // current resolution of the viewing area.
    resolution:[number,number] = [0,0];

    // functions from separate classes to render
    renderFunctions:Array<any> = [];

    renderMesh:any;
    renderShader:any;

    mounted:boolean = false;

    thumbnails:Array<any> = [];

    preview:ThumbnailController;

    setupDone:boolean = false;
    
    constructor(vue) {

        this.vue = vue;

        // build a WebGL renderer.
        this.gl = createRenderer();

        // when DOM is mounted, set up GL stuff
        vue.$on(events.MOUNTED, e =>{
            this.setup();

            // make sure canvas re-sizes.
            this._resize();

            // init animation frame.
            this.run();

        });

        // fire off certain things when the page is re-sized.
        vue.$on(events.RESIZE, e =>{
            this._resize();
        });

        // TODO testing
        this.vue["renderFunctionList"] = this.renderFunctions;

        // attach canvas to body
        this.vue.$el.appendChild(this.gl.canvas);

        this._loadThumbnails();

    }


    /**
     * perform any setup
     */
    setup(){

        // TODO figure out why setup is getting called twice - something to do with Vue mounting.
        if(this.setupDone){
            return;
        }

        this.setupDone = true;

        // setup mesh to render final quad that shows off composite scene.
        this.renderShader = createShader(this.gl,new ShaderFormat(compositeVert,compositeFrag));
        this.renderMesh = new Mesh(this.renderShader,new Quad());

        // setup camera
        this.camera = new PerspectiveCamera({
            near:0.1,
            far:10000.0,
            aspect: this.gl.canvas.width / this.gl.canvas.height,
        });


        this.camera.setZoom(-300);

        // attach main camera onto gl context as a matter of convenience
        this.gl.camera = this.camera;
    }


    /**
     * Main rendering function.
     */
    run(){
        let vue = this.vue;
        let gl = this.gl;
        let compositeMesh = this.renderMesh;


        vue.$on("THUMBS",() =>{
            // initialize preview.



            let timer = setInterval(() =>{
                if(this.gl.width > 300){
                    this.preview = new ThumbnailController(this.gl,vue,this.thumbnails);

                    animate();
                    clearInterval(timer);
                }
            })

        });


        let animate = () => {
            requestAnimationFrame(animate);

            gl.clearScreen();


            if(compositeMesh){
               //compositeMesh.draw();
                this.preview.draw(this.camera);
            }

        }
    }

    /**
     * Ensures all necessary elements resize as needed.
     * @private
     */
    _resize(){

        // ensure the context resizes.
        this._resizeContext();

        // reset the camera
        //this.camera.resize(this.gl.canvas.width / this.gl.canvas.height);
        this.camera.resize(this.gl.canvas.width / this.gl.canvas.height);

    }

    /**
     * Resize GL context according to container.
     * TODO maybe debounce a bit
     */
    _resizeContext(){
        let canvas = this.gl.canvas;
        let node = canvas.parentElement;
        let styles = getComputedStyle(node);

        canvas.width = parseInt(styles.width);
        canvas.height = parseInt(styles.height);

        this.resolution[0] = canvas.width;
        this.resolution[1] = canvas.height;
        this.camera.resize(this.gl.canvas.width / this.gl.canvas.height);
    }

    /**
     * Loads all necessary thumbnails.
     * @private
     */
    _loadThumbnails(){
        let vue = this.vue;
        let assets = vue.content;

        let total = 0;
        let thumbPaths = [];

        // figure out total thumbnails, thumb paths and arrange things in a way that makes it simpler to keep track of
        // loading order.
        for(let i in assets){
            let proj = assets[i];


            thumbPaths.push({
                id:proj.meta.id,
                title:proj.meta.title,
                path:assets[i].thumb
            });

            total += 1;
        }


        // if we haven't loaded all the thumbanails yet
        if(this.thumbnails.length < total){

            // loop through loaded thumbnails, compare with what paths we need to load and
            // build out a list of all remaining paths that need to be loaded.
            this.thumbnails.forEach(thumb => {
                thumbPaths.forEach((data,i) =>{
                    if(thumb.id === data.id){
                        thumbPaths.splice(i,1);
                    }
                })
            });

            // for all remaining paths, load those thumbnails and make textures out of those images.
            thumbPaths.forEach(data => {


                let image = new Image();
                image.src = data.path;

                image.onload = () =>{


                    this.thumbnails.push({
                        id:data.id,
                        title:data.title,
                        tex:createTexture(this.gl,new TextureFormat(this.gl,{
                            data:image
                        }))
                    });
                };
            })

        }

        // set a timer, when all thumbnails are loaded, emit a local event which will trigger
        // drawing.
        let timer = setInterval(() =>{
            if(this.thumbnails.length === total){
                this.vue.$emit("THUMBS");

                clearInterval(timer);
            }
        })

    }
}

export default GLController;
