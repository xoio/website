import mat4 from '../math/mat4'
import vec3 from '../math/vec3'
import {Camera} from "../interfaces";

/**
 * The basic components of a camera
 */
export class CameraBase implements Camera{
    projectionMatrix:Array<any> = mat4.create();
    viewMatrix:Array<any> = mat4.create();
    near:number;
    far:number;
    fov:number;
    aspect:number;
    eye:Array<any> = vec3.create();
    target:Array<any> = vec3.create();
    center:Array<any> = vec3.create();
    position:Array<any> = vec3.create();
    up:Array<any> = vec3.create();
    zoom:number;
    constructor() {

        this.up[1] = 1;
        mat4.identity(this.viewMatrix);
        mat4.identity(this.projectionMatrix);
    }

    translate(position:[number,number,number]){
       this.eye[0] = position[0];
       this.eye[1] = position[1];

       // so we can maintain z position, z position will stay the same if position parameter z position is 0
       this.eye[2] = position[2] !== 0 ? position[2] : this.eye[2];
       mat4.identity(this.viewMatrix);
       mat4.lookAt(this.viewMatrix,this.eye,this.center,this.up)
    }

    cameraLookAt(){}

    getX(){ return this.eye[0]; }
    getY(){ return this.eye[1]; }
    getZ(){ return this.eye[2]; }

    setZoom(val:number){
        this.eye[2] = val;

        mat4.lookAt(this.viewMatrix,this.eye,this.center,this.up)
    }

}

/*
 setPosition(x,y){
        this.eye[0] = x;
        this.eye[1] = y;

        this.viewMatrix = mat4.lookAt(this.viewMatrix,this.eye,this.center,this.up);
    }

    translate(vector:Array<any>,target:[number,number,number] = [0,0,0]){

        this.position[0] = vector[0];
        this.position[1] = vector[1];
        this.position[2] = vector[2];

        this.cameraLookAt(this.eye,target);
        return this;
    }


    cameraLookAt(eye:Array<number>,aCenter?:[number,number,number]){
        this.eye = vec3.clone(eye);
        this.center = aCenter !== undefined ? vec3.clone(aCenter) : this.center;

        vec3.copy(this.position,eye);
        mat4.identity(this.viewMatrix);
        mat4.lookAt(this.viewMatrix,eye,this.center,this.up)
        return this;
    }

    setZoom(zoom:number){
        this.zoom = zoom;
        this.position = [0,0,zoom];

        this.cameraLookAt([0,0,0])
        mat4.translate(this.viewMatrix,this.viewMatrix,[0,0,zoom]);

        return this;
    }
 */

/**
 * The basic definition of a PerspectiveCamera
 */
export class PerspectiveCamera extends CameraBase{

    constructor({
                    fov = Math.PI / 4,
                    aspect = window.innerWidth / window.innerHeight,
                    near = 0.1,
                    far = 1000.0
                }={}) {
        super();

        this.projectionMatrix = mat4.create();
        this.viewMatrix = mat4.create();
        this.fov = fov;
        this.aspect = aspect;
        this.near = near;
        this.far = far;


        mat4.perspective(this.projectionMatrix,fov,aspect,near,far);

        return this;
    }

    /**
     * Same as resize function
     * @param aspectRatio {number} the new aspect ratio
     */
    updateProjectionMatrix(aspectRatio:number){
        this.resize(aspectRatio);
        return this;
    }

    /**
     * Resizes the projection matrix based on a new aspect ratio.
     * @param aspectRatio {number} the new aspect ratio
     */
    resize(aspectRatio:number){
        this.aspect = aspectRatio;
        mat4.perspective(this.projectionMatrix,this.fov,this.aspect,this.near,this.far);
    
        return this;
    }


}

/**
 * Basic components of an orthographic camera
 */
export class OrthoCamera extends CameraBase{

    constructor(fov:number,aspect:number,near:number,far:number) {
        super();
        this.projectionMatrix = mat4.create();
        this.viewMatrix = mat4.create();
        this.fov = fov;
        this.aspect = aspect;
        this.near = near;
        this.far = far;

        mat4.perspective(this.projectionMatrix,fov,aspect,near,far);

        return this;
    }

}