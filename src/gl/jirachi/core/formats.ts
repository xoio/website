import {TextureSpec,ShaderSpec} from "../interfaces";
import {logError} from "../utils";

/**
 * Helps to define the basic properties of a Texture.
 * Used between standard textures and FBOs
 */
export class TextureFormat implements TextureSpec{
    wrapS:number;
    wrapT:number;
    width:number = 512;
    height:number = 512;
    minFilter:number;
    magFilter:number;
    depthType:number;
    data:any;
    attachments:Array<any>;
    target:number;
    internalFormat:number;
    texelType:number;
    level:number;
    format:number;
    flipY:boolean;

    // a way to help identify an FBO or texture.
    name:string = " ";

    constructor(gl = null,{
        data=null,
        isdepth=false,
        width=512,
        height=512
    }={}){
        if(gl === null){
            logError("Unable to init TextureFormat - no WebGL context")
            return;

        }
        this.wrapS = gl.CLAMP_TO_EDGE;
        this.wrapT = gl.CLAMP_TO_EDGE;
        this.width = width;
        this.height = height;
        this.minFilter = gl.NEAREST;
        this.magFilter = gl.NEAREST;
        this.internalFormat = gl.RGB;
        this.format = gl.RGB;
        this.texelType = gl.UNSIGNED_BYTE;
        this.level = 0;
        this.flipY = true;
        this.attachments = [];
        this.data = null;

        if(data !== null){
            this.data = data;
        }
        if(isdepth){

        }else{
            this.target = gl.TEXTURE_2D;
        }

        return this;
    }

    /**
     * Sets the internal format for the texture.
     * @param val {number} the webgl enum of the internal format type.
     */
    setInternalFormat(val:number){
        this.internalFormat = val;
        return this;
    }

    /**
     * Toggles whether or not the texture should be flipped on the y axis.
     */
    toggleFlipY(){
        this.flipY = !this.flipY;
        return this;
    }
}

/**
 * This defines the basic attributes of a WebGLProgram
 */
export class ShaderFormat implements ShaderSpec {
    vertex:string;
    fragment:string;
    uniforms:Array<any>;
    attributes:Array<any>;
    name:string = "";
    version:string = "300 es";

    // varying names for transform feedback.
    varyings:Array<string> = [];

    feedbackMode:number = null;

    constructor(vertex=null,fragment=null) {

        if(vertex instanceof Array){
            this.vertexSources(vertex);
        }else{
            this.vertex = vertex !== null ? vertex : "";
        }

        if(fragment instanceof Array){
            this.fragmentSources(fragment);
        }else {
            // for fragment shaders we sometimes might not need the fragment stage(IE transform feedback), make sure to output default
            // fragment shader.
            this.fragment = fragment !== null ? fragment : "precision highp float; out vec4 glFragColor;\n void main(){glFragColor = vec4(1.);}";
        }

        this.uniforms = [];
        this.attributes = [];
        return this;
    }

    setVaryings(outputs:Array<string>){
        this.varyings = outputs;
        return this
    }

    /**
     * Sets the transform feedback mode
     * @param mode {number} GLenum specifying the feedback mode, either gl.SEPARATE_ATTRIBS or gl.INTERLEAVED_ATTRIBS
     */
    setFeedbackMode(mode:number){
        this.feedbackMode = mode;
        return this;
    }

    /**
     * Sets the vertex source
     * @param source {string} source for the vertex shader
     */
    vertexSource(source:string){
        this.vertex = source;
        return this;
    }

    /**
     * Sets the vertex source
     * @param sources {Array} an array of strings that contain glsl code. All chunks without a "main" function are assumed
     * to be supplemental and appended just after the #version directive
     */
    vertexSources(sources:Array<string>){

        let main = "";
        let others = "";


        sources.forEach(shader =>{
            // look for the actual fragment shader vs supporting functions.
            if(shader.search("void main") !== -1){
                main = shader;
            }else{
                others += shader + "\n";
            }
        });


        // in that main shader, make sure we append other source code after precision calls.
        let mainSplit = main.split('\n');

        // figure out where to insert code, we want to place after precision calls.
        let startIndex = 0;
        for(let i = 0; i < mainSplit.length; ++i){
            let chunk = mainSplit[i];
            if(chunk.search("#version") === -1){
                startIndex = i;
                break;
            }
        }



        // append other sources.
        mainSplit.splice(startIndex,0,others);

        this.vertex = mainSplit.join("\n")
        return this;
    }

    /**
     * Sets the fragment source
     * @param source {string} source for the fragment shader.
     */
    fragmentSource(source:string){
        this.fragment = source;
        return this;
    }

    /**
     * Processes a set of source files for fragment shaders.
     * @param sources {Array} an array of strings that contain glsl code. All chunks without a "main" function are assumed
     * to be supplemental and appended just after all precision directives
     */
    fragmentSources(sources:Array<string>){
        let main = "";
        let others = "";


        sources.forEach(shader =>{
            // look for the actual fragment shader vs supporting functions.
            if(shader.search("void main") !== -1){
                main = shader;
            }else{
                others += shader + "\n";
            }
        });


        // in that main shader, make sure we append other source code after precision calls.
        let mainSplit = main.split('\n');

        // figure out where to insert code, we want to place after precision calls.
        let startIndex = 0;
        for(let i = 0; i < mainSplit.length; ++i){
            let chunk = mainSplit[i];
            if(chunk.search("precision") === -1){
                startIndex = i;
                break;
            }
        }


        // append other sources.
        mainSplit.splice(startIndex,0,others);

        this.fragment = mainSplit.join("\n");
        //this.fragment = sources.join("\n");
        return this;
    }

    /**
     * Sets a uniform that should be a part of the shader.
     * @param uni
     */
    uniform(uni){
        this.uniforms.push(uni);
        return this
    }

    /**
     * Sets the shader version.
     * @param ver
     */
    setVersion(ver:string){
        this.version = ver;
        return this;
    }
}