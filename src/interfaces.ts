
// function implementation for building DOM t
interface TreeFunction {
    (vue:any):void;
}
// function implementation for rendering function
interface RenderFunction {
    (h:any,vue:any);
}

interface MountedFunction {
    ():void;
}

// interface for defining simple Vue Components.
export interface Component {
    // describe interface for tree building function
    tree:TreeFunction;

    // desccribe interface for render function.
    render:RenderFunction;

    mounted:MountedFunction;
}
