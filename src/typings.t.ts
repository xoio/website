declare module '*.glsl' {
    const content:string;
    export default content;

}

declare module '*.frag' {
    const content:string;
    export default content;
}

declare module '*.vert' {
    const content:string;
    export default content;
}

declare interface ObjectConstructor {
    assign(...objects:Object[]):Object;
}

// overrides on the window object so we can customize it a bit.
interface Window {
    emitter:any;
    viewWidth:number;
    viewHeight:number;
    mousePos:any;
    router:any;
    state:any;
    Typekit:any;
    TweenMax:any;
    SIMD:any;

    addResizeCallback:Function;
}

declare module Typekit {
    export function load(opts:Object):void;
}

const TweenMax:any = window["TweenMax"];

/*
 active:any;
    inactive:any;
    fontloading:any;
    fontactive:any;
    fontinactive:any;
 */

